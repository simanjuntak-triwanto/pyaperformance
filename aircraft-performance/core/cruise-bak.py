#!/usr/bin/env python3

import numpy as np
from core.isamodel import isa, rho0

g_si = 9.81


def range_prop_fuelfrac(eta_j, c_p, CLCD, W1W2):
    """
    Compute the range of a/c under propeller propulsion,
    using Breguet formula.

    Ref. Ruijgrok Eq. 15.13
    Keyword Arguments:
    eta_j   -- Propulsive efficiency
    c_p     -- Specific fuel consumption
    CL3CD2  -- CL^3/CD^2
    W1W2    -- Ratio of initial and final weight
    """
    range_ = (eta_j / c_p) * CLCD * np.log(W1W2)
    return range_


def range_prop(eta_j, c_p, CLCD, W1, W2):
    """
    Compute the range of a/c under propeller propulsion,
    using Breguet formula.

    Ref. Ruijgrok Eq. 15.13
    Keyword Arguments:
    eta_j   -- Propulsive efficiency
    c_p     -- Specific fuel consumption
    CL3CD2  -- CL^3/CD^2
    W1      -- Initial weight
    w2      -- Final weight
    """
    range_ = (eta_j / c_p) * CLCD * np.log(W1 / W2)
    return range_


def endurance_prop(eta_j, c_p, rho, CL3CD2, S, W1, W2):
    """
    Compute the endurance of a/c under propeller propulsion,
    using Breguet formula.

    Ref. Ruijgrok Eq. 15.14
    Keyword Arguments:
    eta_j   -- Propulsive efficiency
    c_p     -- Specific fuel consumption
    rho     -- Air density
    CL3CD2  -- CL^3/CD^2
    S       -- Area
    W1      -- Initial weight
    W2      -- Final weight
    """
    endurance = (
        (eta_j / c_p)
        * np.sqrt(CL3CD2 / ((1 / S) * (2 / rho)))
        * (2 / np.sqrt(W2) - 2 / np.sqrt(W1))
    )

    return endurance


def range_jet(c_T, S, rho, CLCD, W1, W2):
    """
    Compute range of a/c under jet propulsion,
    using Breguet formula.

    Ref. Ruijgrok Eq. 15.22,
    Keyword Arguments:
    c_T  -- Specific fuel consumption
    S    -- area
    rho  -- Air density
    CLCD -- CL/CD
    W1   -- Initial weight
    W2   -- Final weight
    """
    range_ = (
        (2 / c_T)
        * np.sqrt(2 / (S * rho) * (CLCD**2))
        * (np.sqrt(W1) - np.sqrt(W2))
    )

    return range_


def range_jet2(eta_tot, CLCD, W1, W2):
    """
    Compute the range (m) of a/c under propeller propulsion,
    using Breguet formula.

    Ref. Ruijgrok Eq. 15.29
    Keyword Arguments:
    eta_tot -- Total efficiency
    CLCD    -- CL/CD
    W1      -- Initial weight
    W2      -- Final weight
    """
    H = 4.3e7  # Heating value for hydrocarbon (J/kg)
    g = 9.81

    range_ = eta_tot * (H / g) * CLCD * np.log(W1 / W2)

    return range_


def endurance_jet(c_T, CLCD, W1, W2):
    """
    Compute endurance (s) of a/c under jet propulsion,
    using Breguet formula.

    Ref. Ruijgrok Eq. 15.30
    Keyword Arguments:
    c_T  -- Specific fuel consumption
    CLCD -- CL/CD
    W1   -- Initial weight
    W2   -- Final weight
    """
    endurance = (1 / c_T) * CLCD * np.log(W1 / W2)

    return endurance


def endurance_prop_EV(n, Rt, eta_tot, Volt, C, rho, V, S, CD0, W, k):
    """
    Compute endurance of electric propeller (hour).

    Ref. Traub, Journal of A/C, N0. 48, No. 2, March-April 2011,
    Eq. 9.
    Keyword Arguments:
    n       -- A discharge parameter dependent on the battery
               type and temperature
    Rt      -- Battery hour rating (h)
    eta_tot -- Total efficiency
    Vol     -- Voltage (V)
    C       -- Battery capacity (Ah)
    rho     -- Air density
    V       -- Air speed (m/s)
    S       -- Area (m^2)
    CD0     -- Zero-lift drag coefficient
    W       -- Weight (N)
    k       -- Lift-drag polar coefficient
    """
    # a1 = Rt ** (1 - n)
    # a2 = eta_tot * Vol * C
    # a3 = 0.5 * rho * (U ** 3) * S * CD0
    # a4 = 2 * W ** 2 * k / (rho * U * S)

    # endurance = a1 * (a2 / (a3 + a4)) ** n

    # return endurance

    E = (
        Rt ** (1 - n)
        * (
            (eta_tot * Volt * C)
            / (
                (0.5 * rho * V**3 * S * CD0)
                + (2 * W**2 * k / (rho * V * S))
            )
        )
        ** n
    )

    return E


def V_max_E(W, rho, S, k, CD0):
    """
    Compute the maximum velocity (m/s) for maximum endurance.

    Ref. Traub, Journal of A/C, N0. 48, No. 2, March-April 2011,
    Eq. 11.
    Keyword Arguments:
    W   -- Weight (N)
    rho -- Air density (kg/m^3)
    S   -- Area (m^2)
    k   -- Lift-drag polar coefficient
    CD0 -- Zero-lift drag coefficient
    """
    VE = np.sqrt(((2 * W) / (rho * S)) * np.sqrt(k / (3 * CD0)))

    return VE


def V_max_R(W, rho, S, k, CD0):
    """
    Compute the maximum velocity (m/s) for maximum range.

    Ref. Traub, Journal of A/C, N0. 48, No. 2, March-April 2011,
    Eq. 12.
    Keyword Arguments:
    W   -- Weight (N)
    rho -- Air density (kg/m^3)
    S   -- Area (m^2)
    k   -- Lift-drag polar coefficient
    CD0 -- Zero-lift drag coefficient
    """
    VR = np.sqrt(((2 * W) / (rho * S)) * np.sqrt(k / CD0))

    return VR


def power_bat2(Vol, i):
    """
    Compute power output of battery (W).

    Ref. Traub, Journal of A/C, N0. 48, No. 2, March-April 2011,
    Eq. 6.
    Keyword Arguments:
    Vol -- Voltage of the battery (Volt)
    i   -- Current (ampere)
    """
    P_B = Vol * i

    return P_B


def power_bat(Vol, C, Rt, t, n):
    """
    Compute power output of battery (W).

    Ref. Traub, Journal of A/C, N0. 48, No. 2, March-April 2011,
    Eq. 7.
    Keyword Arguments:
    Vol -- Battery voltage (Volt)
    C   -- Battery Capacity (Ah)
    Rt  -- Battery hour rating (h)
    t   -- Time (h)
    n   -- Battery discharge parameter
    """
    P_B = Vol * (C / Rt) * (Rt / t) ** (1 / n)

    return P_B


def range_prop_EV(n, Rt, eta_tot, Volt, C, rho, V, S, CD0, W, k):
    """
    Compute range of electric propeller (km).

    Ref. Traub, Journal of A/C, N0. 48, No. 2, March-April 2011,
    Eq. 13.
    Keyword Arguments:
    n       -- A discharge parameter dependent on the battery
               type and temperature.
    Rt      -- Battery hour rating (h)
    eta_tot -- Total efficiency
    Volt    -- Voltage (V)
    C       -- Battery capacity (Ah)
    rho     -- Air density (kg/m^3)
    U       -- Air speed (m/s)
    S       -- Wing Area (m^2)
    CD0     -- Zero-lift drag coefficient
    W       -- Weight (N)
    k       -- Lift-drag polar coefficient
    """
    endurance = endurance_prop_EV(n, Rt, eta_tot, Volt, C, rho, V, S, CD0, W, k)
    VR = V_max_R(W, rho, S, k, CD0)

    range_ = endurance * VR * 3.6

    return range_


def Endurance_Max_EV(n, Rt, eta_tot, Volt, C, rho, S, CD0, W, k):
    """
    Compute maximum endurance (hour) for electrical aircraft.

    Ref. Traub, Journal of A/C, N0. 48, No. 2, March-April 2011,
    Eq. 16.
    Keyword Arguments:
    n       -- A discharge parameter dependent on the battery
               type and temperature.
    Rt      -- Battery hour rating (h)
    eta_tot -- Total efficiency
    Volt    -- Voltage (V)
    C       -- Battery capacity (Ah)
    rho     -- Air density (kg/m^3)
    S       -- Wing Area (m^2)
    CD0     -- Zero-lift drag coefficient
    W       -- Weight (N)
    k       -- Lift-drag polar coefficient
    """
    Emax = (
        Rt ** (1 - n)
        * (
            (eta_tot * Volt * C)
            / (
                (2 / np.sqrt(rho * S))
                * CD0**0.25
                * (2 * W * np.sqrt(k / 3)) ** 1.5
            )
        )
        ** n
    )

    return Emax


def Range_Max_EV(n, Rt, eta_tot, Volt, C, rho, S, CD0, W, k):
    """
    Computes maximum range (km) of electrical aircraft.

    Ref. Traub, Journal of A/C, N0. 48, No. 2, March-April 2011,
    Eq. 18.
    Keyword Arguments:
    n       -- A discharge parameter dependent on the battery
               type and temperature.
    Rt      -- Battery hour rating (h)
    eta_tot -- Total efficiency
    Volt    -- Voltage (V)
    C       -- Battery capacity (Ah)
    rho     -- Air density (kg/m^3)
    S       -- Wing Area (m^2)
    CD0     -- Zero-lift drag coefficient
    W       -- Weight (N)
    k       -- Lift-drag polar coefficient
    """
    Rmax = (
        (
            Rt ** (1 - n)
            * (
                (eta_tot * Volt * C)
                / (
                    (1 / np.sqrt(rho * S))
                    * CD0**0.25
                    * (2 * W * np.sqrt(k)) ** 1.5
                )
            )
            ** n
        )
        * np.sqrt((2 * W) / (rho * S) * np.sqrt(k / CD0))
        * 3.6
    )

    return Rmax


def fuelfrac4cruise_endurance_prop1(
    E_cr, W_init, cp_cr, eta_j, S, rho_cr, CL3CD2_max
):
    """
    Keyword Arguments:
    E_cr -- second
    W_cr -- N
    S     -- m^2
    rho   -- kg/m^3
    CD0   -- Zero-lift Drag coeeficient
    k     -- Parabolic lift-drag polar constant
    """
    x1 = eta_j / cp_cr
    x2 = CL3CD2_max
    x3 = 2 / (S * rho_cr)

    x4 = x1 * np.sqrt(x2 / x3)
    x5 = E_cr / x4

    x6 = 2 / np.sqrt(W_init)
    x7 = 0.5 * (x6 + x5)
    W_final = (1 / x7) ** 2

    fuel_fraction = W_final / W_init

    return fuel_fraction


def fuelfrac4cruise_endurance_prop2(
    E_cr, W_final, cp_cr, S, rho_cr, CL3CD2_max
):
    """
    Keyword Arguments:
    E_cr -- second
    W_cr -- N
    S     -- m^2
    rho   -- kg/m^3
    CD0   -- Zero-lift Drag coeeficient
    k     -- Parabolic lift-drag polar constant
    """
    x1 = eta_j / cp_cr
    x2 = CL3CD2_max
    x3 = 2 / (S * rho_cr)

    x4 = x1 * np.sqrt(x2 / x3)
    x5 = E_cr / x4

    x6 = 2 / np.sqrt(W_final)
    x7 = 0.5 * (x6 - x5)
    W_initial = (1 / x7) ** 2

    fuel_fraction = W_final / W_initial

    return fuel_fraction


def fuelfrac4cruise_range_prop(R_cr, cp_cr, eta_j, CLCD_max):
    """
    Keyword Arguments:
    R_cr   -- cruising range (m)
    W_init -- initial weight (N)
    cp_cr  -- crusing specific fuel consumption (1/m)
    S      -- wing area (m^2)
    rho_c  -- cruising air density
    k      -- parabolic lift-drag polar
    """
    WiWf = np.exp(R_cr * cp_cr / (eta_j * CLCD_max))

    fuel_frac = 1 / WiWf

    return fuel_frac


def range_baruna1(Waf, Wf, Wr, WiWf_ltr, eta_j, cp_ltr, CLCD_max):
    """
    Computes range of Baruna-1.

    Keyword Arguments:
    Waf -- airframe weight (N)
    Wf  -- fuel weight (N)
    Wr  -- retardant weight (N)
    """
    W1W0 = 0.990
    W2W1 = 0.990
    W3W2 = 0.995
    W4W3 = 0.980
    W4W0 = W1W0 * W2W1 * W3W2 * W4W3

    W6W5 = WiWf_ltr
    W7W6 = 0.990
    W8W7 = 0.992
    W8W5 = W6W5 * W7W6 * W8W7

    W8W0 = (Waf + Wr) / (Waf + Wf + Wr)
    W5W4 = W8W0 / (W4W0 * W8W5)
    W4W5 = 1 / W5W4

    range_ = range_prop_fuelfrac(eta_j, cp_ltr, CLCD_max, W4W5) / 1e3  # in km

    return range_


def mission_radius_baruna1_ff(
    Waf, Wf, Wr, S, eta_j, cp, aero_ratios, rad_mission, Hc1, Hc2, Hd
):
    """
    Keyword Arguments:
    Waf           -- airframe weight (N)
    Wf            -- fuel weight (N)
    Wr            -- retardant weight (N)
    S             -- wing area (m^2)
    eta_j         -- propeller efficiency
    cp            -- specific fuel consumption (1/m)
    CD0           -- zero-lift drag coefficient
    k             -- parabolic lift-drag polar
    rad_mission   -- mission radius (m)
    Hc1           -- altitude of cruising 1 (m)
    Hc1           -- altitude of cruising 2 (m)
    Hd            -- drop altitude (m)
    """
    # Aero ratios at cruise
    CD0_cr = aero_ratios[0, 0]
    CLCD_max_cr = aero_ratios[0, 1]
    CL3CD2_max_cr = aero_ratios[0, 2]

    # Aero ratios at drop
    CD0_drop = aero_ratios[1, 0]
    CLCD_max_drop = aero_ratios[1, 1]
    CL3CD2_max_drop = aero_ratios[1, 2]

    Wcrew = 2 * 80 * g_si  # crew weight

    W0 = Waf + Wf + Wr + Wcrew
    # W14 = Waf + 0.1
    # W15 = Waf

    cp_cr1 = cp
    cp_ltr = cp
    cp_cr2 = cp
    cp_drop = cp
    cp_ltr3 = cp

    S_drop = 1.16 * S  # wing area when flap extended for dropping

    one_drop = Wr / 3  # 3 drops

    # Fuel fraction estimation based on Roskam Part 1, Table 2.1
    W1W0 = 0.990  # engine start, warm-up
    W2W1 = 0.990  # taxi
    W3W2 = 0.995  # take-off
    W4W3 = 0.980  # climb 1

    # cruising 1
    W4W0 = W1W0 * W2W1 * W3W2 * W4W3
    W4 = W4W0 * W0
    [_, _, rho_cr1, _, _] = isa(Hc1)
    W4W5 = fuelfrac4cruise_range_prop(rad_mission, cp_cr1, eta_j, CLCD_max_cr)
    W5 = W4W5 * W4

    # descent 1
    W6W5 = 0.990
    W6 = W6W5 * W5

    # drop 1 (assuming 1 menit endurance at Hd)
    E_drop = 5 * 60  # in second
    [_, _, rho_drop, _, _] = isa(Hd)
    W7W6 = fuelfrac4cruise_endurance_prop1(
        E_drop, W6, cp_drop, eta_j, S_drop, rho_drop, CL3CD2_max_drop
    )
    W7 = W7W6 * W6 - one_drop

    # loiter 1 (assuming 15 menit endurance at Hd)
    E_ltr1 = 15 * 60
    W8W7 = fuelfrac4cruise_endurance_prop1(
        E_ltr1, W7, cp_drop, eta_j, S, rho_drop, CL3CD2_max_cr
    )
    W8 = W8W7 * W7

    # drop 2 (assuming 1 menit endurance at Hd)
    W9W8 = fuelfrac4cruise_endurance_prop1(
        E_drop, W8, cp_drop, eta_j, S_drop, rho_drop, CL3CD2_max_drop
    )
    W9 = W9W8 * W8 - one_drop

    # loiter 2 (assuming 5 menit endurance at Hd)
    E_ltr2 = E_ltr1
    W10W9 = fuelfrac4cruise_endurance_prop1(
        E_ltr2, W9, cp_drop, eta_j, S, rho_drop, CL3CD2_max_cr
    )
    W10 = W10W9 * W9

    # drop 3 (assuming 1 menit endurance at Hd)
    W11W10 = fuelfrac4cruise_endurance_prop1(
        E_drop, W10, cp_drop, eta_j, S_drop, rho_drop, CL3CD2_max_drop
    )
    W11 = W11W10 * W10 - one_drop

    # climb 2
    W12W11 = 0.980
    W12 = W12W11 * W11

    # cruise 2
    [_, _, rho_cr2, _, _] = isa(Hc2)
    W13W12 = fuelfrac4cruise_range_prop(rad_mission, cp_cr2, eta_j, CLCD_max_cr)

    # decsent 2
    W14W13 = 0.990
    W14 = W13W12 * W14W13 * W12

    # loiter 3
    E_ltr = (
        45 * 60
    )  # FAR loitering requirement for IFR at at 10,000 ft {3048 m}
    [_, _, rho_ltr3, _, _] = isa(3048)
    W15W14 = fuelfrac4cruise_endurance_prop1(
        E_ltr, W14, cp_ltr3, eta_j, S, rho_ltr3, CL3CD2_max_cr
    )
    W16W15 = 0.992  # landing, taxi, shutdown

    W16 = W16W15 * W15W14 * W14

    fuel_reserve = (W16 - (Waf + Wcrew)) / g_si

    return fuel_reserve


def mission_radius_baruna1_ferry(
    Waf,
    Wf,
    S,
    eta_j,
    cp,
    aero_ratios,
    range_ferry,
    Hc1,
):
    """
    Computes reserve fuel (kg) for ferry mission of Baruna-1.

    Keyword Arguments:
    Waf           -- airframe weight (N)
    Wf            -- fuel weight (N)
    S             -- wing area (m^2)
    eta_j         -- propeller efficiency
    cp            -- specific fuel consumption (1/m)
    CD0           -- zero-lift drag coefficient
    k             -- parabolic lift-drag polar
    rad_mission   -- mission radius (m)
    Hc1           -- altitude of cruising 1 (m)
    Hc1           -- altitude of cruising 2 (m)
    Hd            -- drop altitude (m)
    """
    # Aero ratios at cruise
    CD0_cr = aero_ratios[0, 0]
    CLCD_max_cr = aero_ratios[0, 1]
    CL3CD2_max_cr = aero_ratios[0, 2]

    Wcrew = 2 * 80 * g_si  # 2 persons crew weight

    Wr = 0
    W0 = Waf + Wf + Wcrew
    # W14 = Waf + 0.1
    # W15 = Waf

    cp_cr1 = cp
    cp_ltr = cp
    cp_cr2 = cp
    cp_drop = cp
    cp_ltr3 = cp

    one_drop = Wr / 3  # 3 drops

    # Fuel fraction estimation based on Roskam Part 1, Table 2.1
    W1W0 = 0.990  # engine start, warm-up
    W2W1 = 0.990  # taxi
    W3W2 = 0.995  # take-off
    W4W3 = 0.980  # climb 1

    # cruising 1
    W4W0 = W1W0 * W2W1 * W3W2 * W4W3
    W4 = W4W0 * W0
    [_, _, rho_cr1, _, _] = isa(Hc1)
    W4W13 = fuelfrac4cruise_range_prop(range_ferry, cp_cr1, eta_j, CLCD_max_cr)
    W13 = W4W13 * W4

    # decsent 2
    W14W13 = 0.990
    W14 = W14W13 * W13

    # loiter 3
    E_ltr = (
        45 * 60
    )  # FAR loitering requirement for IFR at at 10,000 ft {3048 m}
    [_, _, rho_ltr3, _, _] = isa(3048)
    W15W14 = fuelfrac4cruise_endurance_prop1(
        E_ltr, W14, cp_ltr3, eta_j, S, rho_ltr3, CL3CD2_max_cr
    )
    W16W15 = 0.992  # landing, taxi, shutdown

    W16 = W16W15 * W15W14 * W14

    fuel_reserve = (W16 - (Waf + Wcrew)) / g_si

    return fuel_reserve

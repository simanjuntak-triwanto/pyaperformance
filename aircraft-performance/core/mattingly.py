from core.isamodel import isa
import numpy as np

GAMMA = 1.4
TR = 1.072  # Throttle Ratio

# Atmospheric properties at sea-level altitude
[T0, p0, RHO0, c0, miu0] = isa(0)


def temperature_ratio(h, mach):
    """
    Calculate the temperature ratio based on altitude and Mach number.

    Keyword Arguments:
    - h: Altitude (meters)
    - mach: Mach number

    Returns:
    - Temperature ratio
    """
    [T, _, _, _, _] = isa(h)
    theta0 = (T / T0) * (1 + (GAMMA - 1) / 2 * mach**2)

    return theta0


def pressure_ratio(h, mach):
    """
    Calculate the pressure ratio based on altitude and Mach number.

    Keyword Arguments:
    h    -- Altitude (meters)
    mach -- Mach number
    """
    [_, p, _, _, _] = isa(h)

    theta0 = (p / p0) * (1 + (GAMMA - 1) / 2 * mach**2)

    return theta0


def maximum_thrust_turbofan(thrust_sl, br, h, mach, kind="civil"):
    """
    Calculate maximum thrust based on altitude, bypass ratio, and Mach number.

    Keyword Arguments:
    thrust_sl  -- Static thrust at sea level
    br   -- Bypass ratio
    h    -- Altitude
    mach -- Mach number
    kind -- "civil" or "military"
    """
    theta0 = temperature_ratio(h, mach)
    delta0 = pressure_ratio(h, mach)

    if br < 1 and kind == "civil":
        if theta0 <= TR:
            thrust = thrust_sl * delta0
        elif theta0 > TR:
            thrust = thrust_sl * delta0 * (1 - 3.5 * (theta0 - TR) / theta0)
    if br < 1 and kind == "military":
        if theta0 <= TR:
            thrust = 0.6 * thrust_sl * delta0
        elif theta0 > TR:
            thrust = (
                0.6 * thrust_sl * delta0 * (1 - 3.5 * (theta0 - TR) / theta0)
            )
    if br > 1 and kind == "civil":
        if theta0 <= TR:
            thrust = thrust_sl * delta0 * (1 - 0.49 * np.sqrt(mach))
        elif theta0 > TR:
            thrust = (
                thrust_sl
                * delta0
                * (
                    1
                    - 0.49 * np.sqrt(mach)
                    - (3 * (theta0 - TR) / (1.5 + mach))
                )
            )

    return thrust

#!/usr/bin/env python3

"""
Reference. Gudmusson, 2022, Chapter 18
"""

import numpy as np

GRAVITY_ACCELERATION = 9.81  # m/s^2


def distance_constant_acceleration(acceleration, final_speed, initial_speed=0):
    """
    Approximate the ground run distance when average acceleration
    and airspeed is known; the units must be consistent.

    Keyword Arguments:
    acceleration   -- acceleration (m/s^2 or ft/s^2)
    final_speed    -- final speed (m/s or ft/s)
    initial_speed  -- initial speed (m/s or ft/s)
    """
    return (final_speed**2 - initial_speed**2) / (2 * acceleration)


def thrust_prop(eta_j, power, vel):
    """
    (Imperial Unit) Compute the thrust of propeller enginer for a given airspeed.
    Keyword Arguments:

    eta_j -- propeller efficiency
    power -- power (Watt)
    vel   -- airspeed (m/s)
    """
    return eta_j * power / vel


def rotation_speed(
    aircraft_weight, air_density, wing_area, max_lift_coefficient
):
    """
    Calculate rotation speed during take-off.

    Unless otherwise specified, it is assumed that VR and VLOF are equal.

    Keyword Arguments:
    aircraft_weight      -- aircraft weight (N)
    air_density          -- air density (kg/m^3)
    wing_area            -- wing area (m^2)
    max_lift_coefficient -- maximum CL
    """
    return 1.556 * np.sqrt(
        aircraft_weight / (air_density * wing_area * max_lift_coefficient)
    )


def ground_run_distance(
    aircraft_weight,
    air_density,
    wing_area,
    max_lift_coefficient,
    lift_coefficient_to,
    drag_coefficient_to,
    thrust,
    ground_resistance,
):
    """
    Compute the ground run distance (meter) and acceleration (m/s^2)
    when thrust is given in SI units.

    Keyword Arguments:
    aircraft_weight      -- aircraft weight (N)
    air_density          -- air density (kg/m^3)
    wing_area            -- wing area (m^2)
    max_lift_coefficient -- maximum CL
    lift_coefficient_to  -- CL during take-off
    drag_coefficient_to  -- drag coefficient during take-off
    thrust               -- thrust at VR/sqrt(2) (N)
    ground_resistance    -- ground running resistance
    """
    rotation_speed_vr = rotation_speed(
        aircraft_weight, air_density, wing_area, max_lift_coefficient
    )
    vr_reduced = rotation_speed_vr / np.sqrt(2)

    drag = 0.5 * air_density * vr_reduced**2 * wing_area * drag_coefficient_to
    lift = 0.5 * air_density * vr_reduced**2 * wing_area * lift_coefficient_to

    acceleration_rate = (GRAVITY_ACCELERATION / aircraft_weight) * (
        thrust - drag - ground_resistance * (aircraft_weight - lift)
    )

    ground_run_distance = distance_constant_acceleration(
        acceleration_rate, rotation_speed_vr
    )

    return ground_run_distance


def time_to_liftoff(SG, accell):
    """
    Calculate time to lift-off (second).

    Keyword Arguments:
    SG     -- ground run distance (ft or meter)
    accell -- accelelation (ft/s^ or m/s^2)
    """
    t_lof = np.sqrt(2 * SG / accell)

    return t_lof


def thrust_average_jet(Tmax, BPR):
    """
    Compute the average of thrust for jet engine (lbf or N);
    the unit must be consistent.

    Keyword Arguments:
    Tmax -- maximum static thrust
    BPR  -- turbofan bypass ratio
    """
    T_avg = 0.75 * Tmax * (5 + BPR) / (4 + BPR)

    return T_avg


def thrust_average_prop(Dp, Pmax, Ne, sigma):
    """
    Compute the average of thrust (Newton) for turboprop in SI units.

    Keyword Arguments:
    Dp    -- propeller diameter (m)
    Pmax  -- maximum total engine power, in Watt.
    Ne    -- number of engines
    sigma -- density ratio

    Kp :
    = 5.75 for P max in BHP
    = 0.321 for P max in kg*m/s
    """
    Kp = 0.321

    Pmax = Pmax / GRAVITY_ACCELERATION  # Convert power to kg*m/s
    alpha = ((sigma * Ne * Dp**2) / (Pmax)) ** (1 / 3)

    Tbar = Kp * Pmax * alpha * GRAVITY_ACCELERATION

    return Tbar


def balanced_field_length(
    wto,
    s,
    rho,
    cd0,
    k,
    clmax,
    h_obstacle,
    tmax,
    bpr,
    n_engines,
    sigma=1.0,
    delta_sto=200.0,
):
    """
    Calculates the balanced field length (S_BFL) for a turbofan aircraft in SI units (meters).

    Based on the reference by Gudmundsson and  Torrenbeek.

    Keyword Arguments:
    wto        -- Maximum take-off weight (N).
    s          -- Wing area (m^2).
    rho        -- Air density (kg/m^3).
    cd0        -- Zero-lift drag coefficient.
    k          -- Lift coefficient slope.
    clmax      -- Maximum lift coefficient.
    h_obstacle -- Obstacle height (m).
    tmax       -- Total maximum static thrust from all engines (N).
    bpr        -- Bypass ratio of the turbofan engine.
    n_engines  -- Number of engines.
    sigma      -- Air density ratio (default: 1.0).
    delta_sto  -- Inertia distance (m) (default: 200.0).
    """
    # Minimum climb angle based on number of engines

    gamma2_min = {
        2: 0.024,
        3: 0.027,
        4: 0.030,
    }.get(n_engines, np.nan)

    if np.isnan(gamma2_min):
        raise ValueError("Invalid number of engines. Must be 2, 3, or 4.")

    # Calculate key parameters
    vs = np.sqrt(2 * wto / (rho * s * clmax))
    v2 = 1.2 * vs

    miu_prime = 0.01 * clmax + 0.02
    cl2 = 0.694 * clmax

    cd2 = cd0 + k * cl2**2
    d2 = 0.5 * rho * v2**2 * s * cd2

    # Average thrust
    t_avg = thrust_average_jet(tmax, bpr)

    # One-engine inoperative thrust (approximation)
    t_oei = 0.5 * t_avg

    gamma2 = np.arcsin((t_oei - d2) / wto)
    delta_gamma2 = gamma2 - gamma2_min

    # Coefficients for S_BFL calculation
    a = 0.863 / (1 + 2.3 * delta_gamma2)
    b = ((wto / s) / (rho * GRAVITY_ACCELERATION * cl2)) + h_obstacle
    c = 2.7 + 1 / ((t_avg / wto) - miu_prime)
    d = delta_sto / np.sqrt(sigma)

    # Balanced field length
    s_bfl = a * b * c + d

    return s_bfl


def thrust_average_jet(tmax, bpr):
    """
    Calculates the average thrust of a turbofan engine (N).

    This function is assumed to be used within the balanced_field_length function.

    Args:
        tmax (float): Total maximum static thrust from all engines (N).
        bpr (float): Bypass ratio of the turbofan engine.

    Returns:
        float: Average thrust of a single engine (N).
    """

    return 0.75 * tmax * ((5 + bpr) / (4 + bpr))


def transition_distance(
    weight,
    thrust,
    air_density,
    area,
    cd0_takeoff,
    k,
    stall_speed,
    obstacle_height=15.2,
):
    """
    Calculate transition distance.

    Keyword Arguments:
    weight          -- aircraft weight (N).
    thrust          -- total thrust (N).
    air_density     -- air density (kg/m^3).
    area            -- wing area (m^2).
    cd0_takeoff     -- zero-lift drag coefficient during takeoff.
    k               -- coefficient related to induced drag.
    stall_speed     -- stall speed (m/s).
    obstacle_height -- obstacle height (m).
    """
    n_tr = 1.1903

    transition_speed = 1.15 * stall_speed
    CL_tr = 2 * weight / (air_density * transition_speed**2 * area)
    CD_tr = cd0_takeoff + k * CL_tr**2
    LD_ratio = CL_tr / CD_tr

    gamma_c = np.arcsin((thrust / weight) - (1 / LD_ratio))

    R_tr = transition_speed**2 / (GRAVITY_ACCELERATION * (n_tr - 1))

    h_tr = R_tr * (1 - np.cos(gamma_c))

    if h_tr < obstacle_height:
        s_tr = R_tr * np.sin(gamma_c)
        s_c = (obstacle_height - h_tr) / np.tan(gamma_c)
        transition_distance = s_tr + s_c
    elif h_tr >= obstacle_height:
        s_tr = np.sqrt(R_tr**2 - (R_tr - obstacle_height) ** 2)
        transition_distance = s_tr

    return transition_distance


def transition_distance2(
    aircraft,
    air_density,
    stall_speed,
    obstacle_height=15.2,
):
    """
    Calculate transition distance.

    Keyword Arguments:
    weight          -- aircraft weight (N).
    thrust          -- total thrust (N).
    air_density     -- air density (kg/m^3).
    area            -- wing area (m^2).
    cd0_takeoff     -- zero-lift drag coefficient during takeoff.
    k               -- coefficient related to induced drag.
    stall_speed     -- stall speed (m/s).
    obstacle_height -- obstacle height (m).
    """
    n_tr = 1.1903

    weight = aircraft.max_takeoff_weight
    thrust = aircraft.max_thrust
    area = aircraft.wing_area
    cd0_takeoff = aircraft.drag_coefficient_zero_lift
    k = aircraft.induced_drag_factor

    transition_speed = 1.15 * stall_speed
    CL_tr = 2 * weight / (air_density * transition_speed**2 * area)
    CD_tr = cd0_takeoff + k * CL_tr**2
    LD_ratio = CL_tr / CD_tr

    gamma_c = np.arcsin((thrust / weight) - (1 / LD_ratio))

    R_tr = transition_speed**2 / (GRAVITY_ACCELERATION * (n_tr - 1))

    h_tr = R_tr * (1 - np.cos(gamma_c))

    if h_tr < obstacle_height:
        s_tr = R_tr * np.sin(gamma_c)
        s_c = (obstacle_height - h_tr) / np.tan(gamma_c)
        transition_distance = s_tr + s_c
    elif h_tr >= obstacle_height:
        s_tr = np.sqrt(R_tr**2 - (R_tr - obstacle_height) ** 2)
        transition_distance = s_tr

    return transition_distance


def rotation_distance(
    takeoff_weight, air_density, wing_area, max_lift_coefficient, size="large"
):
    """
    Compute the rotation distance (m) in SI unit, by using
    V_LOF and estimated rotation time.

    Keyword Arguments:
    takeoff_weight       -- take-off weight (N)
    air_density          -- air density (kg/m^3)
    wing_area            -- wing area (m^2)
    max_lift_coefficient -- maximum CL
    size                  -- determines the rotation time (options: "small" or "large")
    """
    rotation_time = 3 if size == "large" else 1

    V_LOF = 1.1 * np.sqrt(
        2 * takeoff_weight / (air_density * wing_area * max_lift_coefficient)
    )

    rotation_distance = V_LOF * rotation_time

    return rotation_distance


def takeoff_distance(
    takeoff_weight,
    rho0,
    wing_area,
    cl_max,
    cl_to,
    cd_to,
    thrust_at_reduced_vr,
    thrust_at_transition,
    miu,
    cd0_to,
    k,
    stall_speed,
    size,
    obstacle_height,
):
    """
    Calculate the total takeoff distance.

    Keyword Parameters:
        weight (float)               -- Aircraft takeoff weight in Newtons.
        density (float)              -- Air density in kg/m^3.
        wing_area (float)            -- Wing area in square meters.
        cl_max (float)               -- Maximum coefficient of lift.
        cl_to (float)                -- Coefficient of lift during takeoff.
        cd_to (float)                -- Coefficient of drag during takeoff.
        thrust_at_reduced_vr (float) -- Thrust during ground roll.
        miu (float)                  -- Coefficient of friction between tires and runway.
        cd0_to (float)               -- Parasitic drag coefficient during takeoff.
        k (float)                    -- Induced drag factor.
        stall_speed (float)          -- Stall speed in m/s.
        size (str)                   -- Size parameter for rotation distance calculation.
                                        Valid values are "small" or "large".
        obstacle_height (float)      -- Obstacle height (m).

    Returns:
        float: Total takeoff distance in meters.
    """

    ground_dist = ground_run_distance(
        takeoff_weight,
        rho0,
        wing_area,
        cl_max,
        cl_to,
        cd_to,
        thrust_at_reduced_vr,
        miu,
    )
    rot_dist = rotation_distance(takeoff_weight, rho0, wing_area, cl_max, size)

    trans_dist = transition_distance(
        takeoff_weight,
        thrust_at_transition,
        rho0,
        wing_area,
        cd0_to,
        k,
        stall_speed,
    )

    return ground_dist + rot_dist + trans_dist

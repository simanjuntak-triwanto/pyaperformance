#!/usr/bin/env python3

import numpy as np


def rate_of_climb(thrust, velocity, weight, wing_area, air_density, cl3_cd2):
    """
    Compute the rate of climb of an aircraft (m/s).

    Args:
    thrust: Thrust (N)
    velocity: Velocity (m/s)
    weight: Weight (N)
    wing_area: Area of wing (m^2)
    air_density: Air density (kg/m^3)
    cl3_cd2: Ratio CL^3/CD^2

    Returns:
    Rate of climb (m/s)
    """
    power_available = thrust * velocity
    rate_of_climb_result = (power_available / weight) - np.sqrt(
        (weight / wing_area) * (2 / air_density) * (1 / cl3_cd2)
    )
    return rate_of_climb_result


def maximum_level_flight_speed(
    aircraft_weight,
    wing_area,
    thrust,
    zero_lift_drag_coefficient,
    k_factor,
    air_density,
):
    """
    Keyword Arguments:
    aircraft_weight            -- Aircraft weight (N)
    wing_area                  -- Area of wing (m^2)
    thrust                     -- Aircraft thrust (N)
    zero_lift_drag_coefficient -- Zero l;ift drag coefficient
    k_factor                   -- Polar drag factor
    air_density                -- Air density (kg/m^3)
    """
    p1 = thrust / (air_density * zero_lift_drag_coefficient * wing_area)
    p2 = np.sqrt(
        4
        * zero_lift_drag_coefficient
        * k_factor
        * (aircraft_weight / thrust) ** 2
    )

    low_speed = np.sqrt(p1 * (1 + p2))
    high_speed = np.sqrt(p1 * (1 - p2))

    return (low_speed, high_speed)

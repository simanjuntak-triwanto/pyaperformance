#!/usr/bin/env python3

import numpy as np

# ISA at sea level
p0 = 101325  # Pa
T0 = 288.15  # K (15 C)
rho0 = 1.225  # kg/m^3
g0 = 9.80665  # m/s^2
Ra = 8314.32  # Universal gas constant, J/K kmol
gamma = 1.4  # Specific heat ratio for air
R = 287
beta = 1.458e-6  # viscosity constant, kg s^-1 m^-1 K^-0.5
S = 110.4  # Sutherland constant (K)
Re = 6.4e6  # Radius of the earth (m)


def grav_accel(h):
    """
    Return gravity acceleration (m/s^2) for given geometric altitude (h).
    Keyword Arguments:
    h -- Geometric altitude (m)
    """
    g = g0 * Re ** 2 / (Re + h) ** 2

    return g


def speed_sound(T):
    """
    Calculate speed of sound (m/s) for given temperature.

    Keyword Arguments:
    T -- Temperature (K)
    """
    c = np.sqrt(gamma * R * T)

    return c


def geop2geop(H):
    """
    Converts the geometric altitude (m) to geopotential altitude (m).
    Keyword Arguments:
    H -- Geopotential altitude
    """
    h = Re * H / (Re - H)

    return h


def geom2geop(h):
    """
    Converts the geopotential altitude (m) to geometric altitude (m).
    Keyword Arguments:
    h -- Geometric altitude (m)
    """
    H = Re * h / (Re + h)

    return H


def viscosity(T):
    """
    Returns the viscosity (N-S/m^2) of ISA model.

    Keyword Arguments:
    T -- Temperature (K)
    """
    miu = beta * T ** 1.5 / (T + S)

    return miu


def rho_ideal(p, T):
    """
    Returns the air density (kg/m^3) for the given
    pressure and temperature.

    Keyword Arguments:
    p -- Pressure (N/m^3)
    T -- Temperature (K)
    """
    rho = p / (R * T)

    return rho


def atmos_layer(H):
    """
    Determine the layer of the ISA model from the given
    geopotential altitude and then return temperature base and
    gradient (Kelvin/meter) at that layer.

    Data are taken from Figure 3.4 (Introduction to Flight, Anderson [2014])

    Keyword Arguments:
    H -- Geopotential altitude (km)
    """

    assert H >= -5e3 and H <= 80e3, "H must be within the range."

    # Properties of the layers
    layerProp = [
        # H,     T,      Lapse rate, Pressure, Air Density, Layer's name
        [-5.0e3, 320.65, -6.5e-3, 1.77687e5, "Lower Tropospher"],
        [0.00e3, 288.15, -6.5e-3, 1.01325e5, "Troposphere"],
        [11.0e3, 216.65, 0.0e-3, 2.26320e4, "Tropopause"],
        [20.0e3, 216.65, 1.0e-3, 5.47487e3, "Lower Stratosphere"],
        [32.0e3, 228.65, 2.8e-3, 8.68014e2, "Upper Stratosphere"],
        [47.0e3, 270.65, 0.0e-3, 1.10906e2, "Stratopause"],
        [51.0e3, 270.65, -2.8e-3, 6.69384e1, "Lower Mesosphere"],
        [71.0e3, 214.65, -2.0e-3, 3.95639e0, "Upper Mesosphere"],
        [80.0e3, 196.65, 0.0e-3, 8.86272e-1, "Mesopause"],
    ]

    # Atmosphere layers' altitude (km)
    H_0 = layerProp[0][0]
    H_1 = layerProp[1][0]
    H_2 = layerProp[2][0]
    H_3 = layerProp[3][0]
    H_4 = layerProp[4][0]
    H_5 = layerProp[5][0]
    H_6 = layerProp[6][0]
    H_7 = layerProp[7][0]
    H_8 = layerProp[8][0]

    # Determine the temperature gradient, T, and p at the base layers.
    # noniso -> non-isothermal layers
    # iso --> isothermal layers.

    # Lower Troposphere
    if H >= -5000 and H < H_1:
        H1 = H_0
        T1 = layerProp[0][1]
        a = layerProp[0][2]
        p1 = layerProp[0][3]
        lname = layerProp[0][4]
        kind = "noniso"

    # Upper Troposphere
    elif H >= H_1 and H < H_2:
        H1 = H_1
        T1 = layerProp[1][1]
        a = layerProp[1][2]
        p1 = layerProp[1][3]
        lname = layerProp[1][4]
        kind = "noniso"

    # Tropopause
    elif H >= H_2 and H < H_3:
        H1 = H_2
        T1 = layerProp[2][1]
        a = layerProp[2][2]
        p1 = layerProp[2][3]
        lname = layerProp[2][4]
        kind = "iso"

    # Lower Stratosphere
    elif H >= H_3 and H < H_4:
        H1 = H_3
        T1 = layerProp[3][1]
        a = layerProp[3][2]
        p1 = layerProp[3][3]
        lname = layerProp[3][4]
        kind = "noniso"

    # Upper Stratosphere
    elif H >= H_4 and H < H_5:
        H1 = H_4
        T1 = layerProp[4][1]
        a = layerProp[4][2]
        p1 = layerProp[4][3]
        lname = layerProp[4][4]
        kind = "noniso"

    # Stratopause
    elif H >= H_5 and H < H_6:
        H1 = H_5
        T1 = layerProp[5][1]
        a = layerProp[5][2]
        p1 = layerProp[5][3]
        lname = layerProp[5][4]
        kind = "iso"

    # Lower Mesosphere
    elif H >= H_6 and H < H_7:
        H1 = H_6
        T1 = layerProp[6][1]
        a = layerProp[6][2]
        p1 = layerProp[6][3]
        lname = layerProp[6][4]
        kind = "noniso"

    # Upper Mesosphere
    elif H >= H_7 and H < H_8:
        H1 = H_7
        T1 = layerProp[7][1]
        a = layerProp[7][2]
        p1 = layerProp[7][3]
        lname = layerProp[7][4]
        kind = "noniso"

    # Mesopause
    elif H >= H_8 and H <= 90000:
        H1 = H_8
        T1 = layerProp[8][1]
        a = layerProp[8][2]
        p1 = layerProp[8][3]
        lname = layerProp[8][4]
        kind = "iso"

    return [H1, T1, a, p1, kind, lname]


def p_isoT(p1, T, dH):
    """
    On the isothermal layer, calculate the pressure at geopotential altitude
    difference from the base layer.

    Keyword Arguments:
    p1 -- Pressure at the base layer (Pa)
    T  -- Temperature (K)
    dH -- Geopotential altitude difference (Km)
    """
    p = p1 * np.exp(-(g0 / (R * T) * dH))

    return p


def p_nonisoT(p1, a, T1, T):
    """
    On the non-isothermal layer, caculate the pressure at the geopotential
    difference from the base layer.
    Keyword Arguments:
    p1 -- Pressure at the base layer (Pa)
    a  -- Gradient temperature (K/km)
    T1 -- Temperature at the base layer (K)
    T  -- Temperature the altitude (K)
    """
    p = p1 * (T / T1) ** (-g0 / (a * R))

    return p


def rho_isoT(rho1, T, dH):
    """
    On the isothermal layer, calculate the air density (kg/m^3) at
    geopotential altitude difference from the base layer.

    Keyword Arguments:
    rho1 -- Air density at the base layer (kg/m^3)
    T    -- Temperature at the altitude (K)
    dH   -- Geopotential altitude difference (Km)
    """
    rho = rho1 * np.exp(-(g0 / (R * T)) * dH)

    return rho


def rho_nonisoT(rho1, a, T, T1):
    """
    Keyword Arguments:
    rho1 -- Air density at the base layer (kg/m^3)
    a    -- Gradient temperature (K/km)
    T    -- Temperature (K)
    T1   -- Temperature at the base layer (K)
    """
    rho = rho1 * (T / T1) ** -((g0 / (a * R)) + 1)

    return rho


def isa(H, dT=0):
    """
    Calculate international standard atmosphere (T, p, rho, c, miu)
    for a given geopotential altitude and temperature deviation.

    Keyword Arguments:
    H  -- Geopotential altitude (km)
    dT -- Temperature deviation (Kelvin)
    """

    [H1, T1, a, p1, kind, lname] = atmos_layer(H)

    # Temperature
    dH = H - H1
    T = T1 + a * dH
    Toff = T + dT

    # Pressure
    # poff = P hence T is used.
    if kind == "noniso":
        p = p_nonisoT(p1, a, T1, T)

    else:
        p = p_isoT(p1, T, dH)

    # Air density
    rho = rho_ideal(p, Toff)

    # Speed of sound
    c = speed_sound(Toff)

    # Viscosity
    miu = viscosity(Toff)

    return [Toff, p, rho, c, miu]


if __name__ == "__main__":
    import matplotlib
    import matplotlib.pyplot as plt

    plt.style.use("ggplot")

    matplotlib.rcParams["text.usetex"] = True

    # Atmospheric properties for 5 km <= H <= 80 km
    Hs = np.arange(0, 80000)
    Ts = np.array([])
    ps = np.array([])
    rhos = np.array([])
    cs = np.array([])
    mius = np.array([])

    for H in Hs:
        [T, p, rho, c, miu] = isa(H)
        Ts = np.append(Ts, T)
        ps = np.append(ps, p)
        rhos = np.append(rhos, rho)
        cs = np.append(cs, c)
        mius = np.append(mius, miu)

    FIG = plt.figure(figsize=(7, 18))
    # Temperature
    ax1 = FIG.add_subplot(311)
    ax1.plot(Hs / 1000, Ts)
    ax1.grid(True)
    ax1.set_ylabel(r"Temperature ($\mathbf{K}$)")
    plt.setp(ax1.get_xticklabels(), visible=False)

    # Air density
    ax2 = FIG.add_subplot(312, sharex=ax1)
    ax2.plot(Hs / 1000, rhos)
    ax2.set_ylabel(r"Density $(\mathbf{kg/m^3})$")
    ax2.grid(True)
    plt.setp(ax2.get_xticklabels(), visible=False)

    # Air pressure
    ax3 = FIG.add_subplot(313, sharex=ax1)
    ax3.plot(Hs / 1000, ps / 1000)
    ax3.set_xlabel(r"Geopotential Altitude ($\mathbf{km}$)")
    ax3.set_ylabel(r"Pressure ($\mathbf{kPa}$)")
    ax3.grid(True)

    plt.tight_layout()
    plt.savefig("isamodel.pdf", dpi=600)
    plt.show()

#!/usr/bin/env python3


def Wtot(j, Wbatt, BR):
    """
    Aircraft total weight (N).

    Ref. Traub, Journal of A/C, N0. 48, No. 2, March-April 2011,
    Eq. 20.
    Keyword Arguments:
    j     -- Number of batteries
    Wbatt -- Weight of each individual battery (N)
    BR    -- The battery weight as a fraction of the total weight,
             typically 0.3 to 0.4
    """
    Wtot = (j * Wbatt) / BR

    return Wtot


def Ctot(j, Cbatt):
    """
    Compute total capacity of the battery (Ah).

    Ref. Traub, Journal of A/C, N0. 48, No. 2, March-April 2011,
    Eq. 19.
    Keyword Arguments:
    j     -- Number of batteries
    Cbatt -- Capacity of each battery (Ah)
    """
    Ctot = j * Cbatt

    return Ctot


def Num_Batt(Ctot, Cbatt):
    """
    Compute number of battery.

    Ref. Traub, Journal of A/C, N0. 48, No. 2, March-April 2011,
    Eq. 19.
    Keyword Arguments:
    Ctot  -- Total capacity of the battery (Ah)
    Cbatt -- Capacity of each battery (Ah)
    """
    j_batt = Ctot / Cbatt

    return j_batt

#!/usr/bin/env python3

# Constants for imperial to SI conversion
FT2M = 0.3048  # feet to meter
LBM2KG = 0.453592  # pound mass to kg
KNOT2MPS = 0.514444  # knot to m/s
LBF2N = 4.44822  # lbf to N
IN2M = 0.0254  # inch to m
IN2MM = 25.4  # inch to mm
NMI2M = 1852  # nautical mile to m
FPM2MPS = 0.00508  # Feet per minute to m/s
FTSQ2MSQ = 0.092903  # Feet square to meter square
FPS2MPS = 0.3048  # Feet per second to m/s
BHP2KW = 0.7457  # BHP to kW
GAL2KG = 3.04  # US Gallons fuel jet A to kg

# Constants for SI to imperial conversion
M2FT = 1 / FT2M
KG2LBM = 1 / LBM2KG
MPS2KNOT = 1 / KNOT2MPS
N2LBF = 1 / LBF2N
M2IN = 1 / IN2M
MM2IN = 1 / IN2MM
M2NMI = 1 / NMI2M
MPS2FPM = 1 / FPM2MPS
MSQ2FTSQ = 1 / FTSQ2MSQ
MPS2FPS = 1 / FPS2MPS
KW2BHP = 1 / BHP2KW
KG2GAL = 1 / GAL2KG


def feet_to_meters(feet):
    """Convert from feet to meters."""
    return FT2M * feet


def pounds_to_kilograms(pounds):
    """Convert from pounds to kilograms."""
    return LBM2KG * pounds


def pounds_force_to_newtons(pounds_force):
    """Convert from newtons to pounds force."""
    return LBF2N * pounds_force


def knot_to_meter_per_second(knot):
    """Convert from knot to meter per second."""
    return KNOT2MPS * knot


def inches_to_meters(inches):
    """Convert from inches to meters."""
    return IN2M * inches


def inches_to_millimeters(inches):
    """Convert from inches to millimeters."""
    return IN2MM * inches


def nautical_miles_to_meters(nautical_miles):
    """Convert from nautical miles to meters."""
    return NMI2M * nautical_miles


def feet_per_minute_to_meters_per_second(feet_per_minute):
    """Convert from feet per minute to meters per second."""
    return FPM2MPS * feet_per_minute


def square_feet_to_square_meters(square_feet):
    """Convert from square feet to square meters."""
    return FTSQ2MSQ * square_feet


def feet_per_second_to_meters_per_second(feet_per_second):
    """Convert from feet per second to meters per second."""
    return FPS2MPS * feet_per_second


def brake_horsepower_to_kilowatts(brake_horsepower):
    """Convert from brake horsepower to kilowatts."""
    return BHP2KW * brake_horsepower


def gallons_to_kilograms(gallons):
    """Convert from US gallons of jet fuel A to kilograms."""
    return GAL2KG * gallons


def meters_to_feet(meters):
    """Convert from meters to feet."""
    return M2FT * meters


def kilograms_to_pounds(kilograms):
    """Convert from kilograms to pounds."""
    return KG2LBM * kilograms


def meters_per_second_to_knots(meters_per_second):
    """Convert from meters per second to knots."""
    return MPS2KNOT * meters_per_second


def newtons_to_pounds_force(newtons):
    """Convert from newtons to pounds force."""
    return N2LBF * newtons


def meters_to_inches(meters):
    """Convert from meters to inches."""
    return M2IN * meters


def millimeters_to_inches(millimeters):
    """Convert from millimeters to inches."""
    return MM2IN * millimeters


def meters_to_nautical_miles(meters):
    """Convert from meters to nautical miles."""
    return M2NMI * meters


def meters_per_second_to_feet_per_minute(meters_per_second):
    """Convert from meters per second to feet per minute."""
    return MPS2FPM * meters_per_second


def square_meters_to_square_feet(square_meters):
    """Convert from square meters to square feet."""
    return MSQ2FTSQ * square_meters


def meters_per_second_to_feet_per_second(meters_per_second):
    """Convert from meters per second to feet per second."""
    return MPS2FPS * meters_per_second


def kilowatts_to_brake_horsepower(kilowatts):
    """Convert from kilowatts to brake horsepower."""
    return KW2BHP * kilowatts


def kilograms_to_gallons(kilograms):
    """Convert from kilograms to US gallons of jet fuel A."""
    return KG2GAL * kilograms

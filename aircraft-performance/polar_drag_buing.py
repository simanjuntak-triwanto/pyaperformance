#!/usr/bin/env python3

import numpy as np
import matplotlib.pyplot as plt
from core.data import Parabolic
import matplotlib

# Set matplotlib parameters for better visualization
plt.rcParams["text.usetex"] = True
plt.style.use("bmh")

CL_MAX = 2.7

lift_drag_polar_takeoff = Parabolic(0.06117, 0.04415)
lift_drag_polar_cruising = Parabolic(0.01941, 0.0419)
lift_drag_polar_landing = Parabolic(0.05957, 0.04028)


def drag_coefficient(parabolic, lift_coefficient):
    """
    To calculate parabolic drag coefficient.

    Keyword Arguments:
    parabolic (LiftDragPolar) -- Dataclass of lift-drag polar
    lift_coefficient (float)  -- Lift coefficient
    """
    cd0 = parabolic.zero_lift_drag_coefficient
    k_factor = parabolic.k_factor

    return cd0 + k_factor * lift_coefficient**2


drag_coefficient_vector = np.vectorize(drag_coefficient)

CLs = np.linspace(0, CL_MAX, 100)
cls_squared = CLs**2

cds_cruising = drag_coefficient_vector(lift_drag_polar_cruising, CLs)
cds_takeoff = drag_coefficient_vector(lift_drag_polar_takeoff, CLs)
cds_landing = drag_coefficient_vector(lift_drag_polar_landing, CLs)

cl_to_cd_ratios_cruising = CLs / cds_cruising
cl_to_cd_ratios_takeoff = CLs / cds_takeoff
cl_to_cd_ratios_landing = CLs / cds_landing

cl3_cd2_ratios_cruising = CLs**3 / cds_cruising**2
cl3_cd2_ratios_takeoff = CLs**3 / cds_takeoff**2
cl3_cd2_ratios_landing = CLs**3 / cds_landing**2


cl_to_cd2_ratios_cruising = CLs / cds_cruising**2
cl_to_cd2_ratios_takeoff = CLs / cds_takeoff**2
cl_to_cd2_ratios_landing = CLs / cds_landing**2

# Plotting
fig, ax = plt.subplots(2, 2, layout="tight", figsize=(12, 10))
fig.tight_layout(pad=5.0)

# Plot CD vs CL
ax[0, 0].plot(cds_cruising, CLs, label="Cruising")
ax[0, 0].plot(cds_takeoff, CLs, label="Takeoff")
ax[0, 0].plot(cds_landing, CLs, label="Landing")
ax[0, 0].set_xlabel(r"$C_D$")
ax[0, 0].set_ylabel(r"$C_L$")
ax[0, 0].legend()

# Plot CL/CD vs CL
ax[0, 1].plot(CLs, cl_to_cd_ratios_cruising, label="Cruising")
ax[0, 1].plot(CLs, cl_to_cd_ratios_takeoff, label="Takeoff")
ax[0, 1].plot(CLs, cl_to_cd_ratios_landing, label="Landing")
ax[0, 1].set_xlabel(r"$C_L$")
ax[0, 1].set_ylabel(r"$\frac{C_L}{C_D}$")
ax[0, 1].legend()

# Plot CL^3/CD^2 vs CL
ax[1, 0].plot(CLs, cl3_cd2_ratios_cruising, label="Cruising")
ax[1, 0].plot(CLs, cl3_cd2_ratios_takeoff, label="Takeoff")
ax[1, 0].plot(CLs, cl3_cd2_ratios_landing, label="Landing")
ax[1, 0].set_xlabel(r"$C_L$")
ax[1, 0].set_ylabel(r"$\frac{C_L^3}{C_D^2}$")
ax[1, 0].legend()

# Plot CL/CD^2 vs CL
ax[1, 1].plot(CLs, cl_to_cd2_ratios_cruising, label="Cruising")
ax[1, 1].plot(CLs, cl_to_cd2_ratios_takeoff, label="Takeoff")
ax[1, 1].plot(CLs, cl_to_cd2_ratios_landing, label="Landing")
ax[1, 1].set_xlabel(r"$C_L$")
ax[1, 1].set_ylabel(r"$\frac{C_L}{C_D^2}$")
ax[1, 1].legend()

fig.savefig("lift-drag-polar-plots-buing.pdf", dpi=300)

plt.show()

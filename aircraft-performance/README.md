
# Table of Contents

1.  [About](#orgbf6a518)
2.  [TODO](#org52158ed)


<a id="orgbf6a518"></a>

# About

This Python library contains various tools for aircraft performance analysis.


<a id="org52158ed"></a>

# TODO

-   [-] ISA Model
    -   [X] Create python package
    -   [ ] Add documentation
    -   [X] Add additional function: geometric altitude to geopotential altitude
    -   [X] Add Mach calculation
    -   [X] Improve plotting
-   [ ] Drag Polar
-   [ ] Power required minimum
-   [ ] Drag minimum
-   [ ] Climbing Flight
-   [ ] Gliding Flight
-   [ ] Ceilings
-   [ ] Cruising Performance
    -   [ ] Range
    -   [ ] Endurance
-   [ ] Gliding Flight
-   [ ] Airfield Performance
-   [ ] Turning Flight


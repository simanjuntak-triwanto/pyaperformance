#!/usr/bin/env python3

import matplotlib.pyplot as plt

from core.aerobasis import (
    aero_max_ratios_using_k_factor,
    compute_CL_at_CLCD2max,
)
from core.cruise import (
    plot_payload_range_curve_jet,
    compute_jet_range,
    compute_effective_endurance_jet,
)
from core.isamodel import isa
from utils.uconverters import feet_to_meters

# Set matplotlib parameters for better visualization
plt.rcParams["text.usetex"] = True
plt.style.use("bmh")


GRAVITY_ACCELERATION = 9.81

# Weights
MTOW = 5_399_943.93  # Maximum takeoff weight (N)
FUEL_WEIGHT_NOMINAL = 1_471_500  # Nominal fuel weight (N)
MAX_FUEL_WEIGHT = 2_300_000  # Maximum fuel weight (N)
MAX_PAYLOAD_WEIGHT = 1_912_950  # Maximum payload weight (N)
EMPTY_OPERATING_WEIGHT = 2_015_493.93  # Empty operating weight (N)

# Aerodynamics cruising
WING_AREA = 728.6  # Wing Area (m^2)
K_FACTOR = 0.0419
CD0 = 0.01941

TSFC = 0.5050188 / 3600  # specific fuel consumption (1/s)
FST_CRUISING_ALTITUDE = feet_to_meters(31_000)
SND_CRUISNG_ALTITUDE = feet_to_meters(15_000)

CL_CD_MAX, _, CL_CD2_MAX = aero_max_ratios_using_k_factor(CD0, K_FACTOR)
CL_at_CLCD2max = compute_CL_at_CLCD2max(CD0, K_FACTOR)

plot_payload_range_curve_jet(
    MAX_PAYLOAD_WEIGHT,
    MTOW,
    FUEL_WEIGHT_NOMINAL,
    MAX_FUEL_WEIGHT,
    WING_AREA,
    CL_CD_MAX,
    CL_CD2_MAX,
    FST_CRUISING_ALTITUDE,
    SND_CRUISNG_ALTITUDE,
    TSFC,
)

_, _, RHO22000, _, _ = isa(FST_CRUISING_ALTITUDE)

my_range = compute_jet_range(
    TSFC, WING_AREA, RHO22000, CL_CD2_MAX, MTOW, MTOW - FUEL_WEIGHT_NOMINAL
)

my_endurance = compute_effective_endurance_jet(
    MTOW,
    FUEL_WEIGHT_NOMINAL,
    WING_AREA,
    CL_CD_MAX,
    CL_CD2_MAX,
    FST_CRUISING_ALTITUDE,
    SND_CRUISNG_ALTITUDE,
    TSFC,
)

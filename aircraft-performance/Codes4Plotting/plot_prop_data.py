import numpy as np
import matplotlib.pyplot as plt
import matplotlib

matplotlib.rcParams["text.usetex"] = True
plt.style.use("bmh")

# Filename for data
data_file = "propdata.csv"

# Load data from CSV
data = np.genfromtxt(data_file, skip_header=1, delimiter=",")

# Extract altitude data (meters)
altitude_levels = data[0][1:]
num_altitudes = int(altitude_levels.shape[0] / 2)

# Extract Mach number data
mach_numbers = data[:, 0][1:]
num_mach_numbers = mach_numbers.shape[0]

# Extract pressure ratio data
pressure_ratios = data[1: num_mach_numbers + 1, 1:]

# Extract thrust data (kN)
thrusts = data[1: num_mach_numbers + 1, 11:]

# Create a figure and axis for plotting
fig, ax = plt.subplots(figsize=(10, 10))

# Plot thrust vs Mach number for each altitude
for altitude_index in np.arange(num_altitudes):
    label_name = f"Altitude = {int(altitude_levels[altitude_index])} m"
    ax.plot(mach_numbers, thrusts[:, altitude_index], 'o', label=label_name)

# Set axis labels
ax.set_xlabel(r"$M$")
ax.set_ylabel(r"$kN$")

# Create legend
ax.legend(bbox_to_anchor=(1.05, 1.0), loc='upper left')

# Adjust layout to prevent overlapping elements
plt.tight_layout()

# Save plot as PDF with high resolution and tight bounding box
fig.savefig(data_file[:-4] + ".pdf", dpi=300, bbox_inches="tight")

# Display plot on screen
plt.show()

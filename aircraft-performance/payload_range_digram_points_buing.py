#!/usr/bin/env python3

import numpy as np

import matplotlib.pyplot as plt


from core.aerobasis import aero_max_ratios_using_AR, compute_CL_at_CLCD2max
from core.cruise import (
    plot_payload_range_points_jet,
)  # compute_effective_range_jet
from utils.uconverters import (
    pounds_to_kilograms,
    square_feet_to_square_meters,
    feet_to_meters,
    pounds_force_to_newtons,
)


GRAVITY_ACCELERATION = 9.81

WING_AREA = square_feet_to_square_meters(318)  # m^2
WING_SPAN = feet_to_meters(53.3)  # m
MTOW = pounds_to_kilograms(2950) * GRAVITY_ACCELERATION  # Newton
NOMINAL_FUEL_WEIGHT = pounds_force_to_newtons(800)
MAX_FUEL_WEIGHT = 4270  # Maximum Fuel Weight Capacity
MAX_PAYLOAD = 1900  # Maximum Payload (N)
OPERATING_EMPTY_WEIGHT = MTOW - (MAX_PAYLOAD + NOMINAL_FUEL_WEIGHT)

TOTAL_THRUST = 2 * pounds_force_to_newtons(3650)
C_T = 0.6 / 3600  # specific fuel consumption (1/s)
FST_CRUISING_ALTITUDE = feet_to_meters(22000)
SND_CRUISNG_ALTITUDE = feet_to_meters(15000)

CD0 = 0.02
OSWALD_EFFICIENCY = 0.81
ASPECT_RATIO = WING_SPAN**2 / WING_AREA
K_FACTOR = 1 / (np.pi * ASPECT_RATIO * OSWALD_EFFICIENCY)
CL_CD_MAX, CL3_CD2_MAX, CL_CD2_MAX = aero_max_ratios_using_AR(
    CD0, ASPECT_RATIO, OSWALD_EFFICIENCY
)
CL_at_CLCD2max = compute_CL_at_CLCD2max(CD0, K_FACTOR)


plot_payload_range_points_jet(
    MAX_PAYLOAD,
    MTOW,
    NOMINAL_FUEL_WEIGHT,
    MAX_FUEL_WEIGHT,
    WING_AREA,
    CL_CD_MAX,
    CL_CD2_MAX,
    FST_CRUISING_ALTITUDE,
    SND_CRUISNG_ALTITUDE,
    C_T,
    fname_out="payload_range_jet_simple.pdf",
)

#!/usr/bin/env python

# Statistical method for estimating aircraft weight components using
# formulas developed by Raymer.
# See: Aircraft Design: A Conceptual Approach by Daniel P. Raymer,
# Section 15.3.3
#
# General Aviation Weights (British Units, Results i n Pounds).

import numpy as np


## -------------------------1. WING----------------------------------------------
def wing_weight(sw, wfw, arw, sweep4_wing, q, tr_wing, t2c_w, nz, w0):
    """
    Estimate wing weight (lb) using Raymer formula (Eq. 15.46);

    NOTE: ignore second term if wfw = 0;

    Keyword Arguments:
    sw           -- Trapezoidal wing area in (ft2);
    wfw          -- Weight of fuel in wing in lb. (If wfw=0 then let
                    w^0.0035=1);
    arw          -- Aspect Ratio of wing;
    sweep4_wing  -- Wing sweep at 25% MGC;
    q            -- Dynamic pressure at cruise (lbf/ft2);
    tr_wing      -- Taper ratio of wing;
    t2c_w        -- Wing thickness-to-chord ratio (maximum);
    nz           -- Ultimate load factor (= 1.5 x limit load factor);
    w0           -- Design gross weight (lb).
    """
    if wfw == 0:
        wfw_ = 1
    else:
        wfw_ = wfw
    w_w = (
        0.036
        * sw**0.758
        * wfw_**0.0035
        * (arw / np.cos(sweep4_wing) ** 2) ** 0.6
        * q**0.006
        * tr_wing**0.004
        * (100 * t2c_w / np.cos(sweep4_wing)) ** (-0.3)
        * (nz * w0) ** 0.49
    )

    return w_w


## -------------------------2. Horizontal Tail-----------------------------------
def ht_weight(nz, w0, q, sht, t2c_wing, arw, sweep4_ht, sweep4_wing, tr_ht):
    """
    Estimate the horizontal tail weight using Raymer formula.

    Keyword Arguments:
    nz         -- Ultimate load factor (= 1.5 x limit load factor);
    w0         -- Design gross weight (lb);
    q          -- Dynamic pressure at cruise (lbf/ft2);
    sht        -- Trapezoidal HT area in ft2;
    t2c_wing   -- Wing thickness-to-chord ratio (maximum);
    arw        -- Aspect Ratio of wing;
    sweep4_ht  -- HT sweep ratio at 25% MGC;
    tr_ht      -- HT taper ratio.
    """

    w_ht = (
        0.016
        * (nz * w0) ** 0.414
        * q**0.168
        * sht**0.896
        * (100 * t2c_wing / np.cos(sweep4_wing)) ** -0.12
        * (arw / np.cos(sweep4_ht) ** 2) ** 0.043
        * tr_ht**-0.02
    )

    return w_ht


## -------------------------3. Vertical Tail-------------------------------------
def vt_weight(f_tail, nz, w0, q, svt, t2c_wing, sweep4_vt, arw, tr_vt):
    """
    Estimate the horizontal tail weight using Raymer formula.

    Keyword Arguments:
    f_tail     -- 0 for conventional tail, 1 for T-tail;
    nz         -- Ultimate load factor (= 1.5 x limit load factor);
    w0         -- Design gross weight (lb);
    q          -- Dynamic pressure at cruise (lbf/ft2);
    svt        -- Trapezoidal VT area in ft2;
    t2c_wing   -- Wing thickness-to-chord ratio (maximum);
    sweep4_vt  -- VT sweep at 25% MGC.
    ar_vt      -- AR of VT;
    tr_vt      -- VT taper ratio.
    """
    if tr_vt < 0.2:
        tr_vt_ = 0.2
    else:
        tr_vt_ = tr_vt

    w_vt = (
        0.073
        * (1 + 0.2 * f_tail)
        * (nz * w0) ** 0.376
        * q**0.122
        * svt**0.873
        * (100 * t2c_wing / np.cos(sweep4_vt)) ** (-0.49)
        * (arw / np.cos(sweep4_vt) ** 2) ** 0.357
        * tr_vt_**0.039
    )

    return w_vt


## -------------------------4. Fuselage------------------------------------------
def fus_weight(sfus, nz, w0, lht, lfs, dfs, q, vp, delta_p=8):
    """
    Estimate fuselage weight using Raymer formula.

    Keyword Arguments:
    sfus    -- Fuselage wetted area in ft2
    nz      -- Ultimate load factor (= 1.5 x limit load factor);
    w0      -- Design gross weight (lb);
    lht     -- Horizontal tail arm, from wing c/4 to HT c/4 in ft;
    lfs     -- Length of fuselage structure (forward bulkhead to aft frame) in ft;
    dfs     -- Depth of fuselage structure in ft;
    q       -- Dynamic pressure at cruise (lbf/ft2);
    vp      -- Volume of pressurized cabin section in ft3;
    delta_p -- (default 8 psi) Cabin pressure differential, in psi (typically 8 psi).
    """

    w_fus = (
        0.052
        * sfus**1.086
        * (nz * w0) ** 0.177
        * lht**-0.051
        * (lfs / dfs) ** -0.072
        * q**0.241
        + 11.9 * (vp * delta_p) ** 0.271
    )

    return w_fus


## -------------------------5. Main Landing Gear---------------------------------
def mnlg_weight(wl, lm, nl=4.5):
    """
    Estimate main LG weight using Raymer formula.

    Keyword Arguments:
    wl -- Design landing weight in lb;
    lm -- Length of the main landing gear shock strut in ft;
    nl -- (default 4.5) Ultimate landing load factor (typical range 3.5–5.5).
    """

    w_mnlg = 0.095 * (nl * wl) ** 0.768 * lm**0.409

    return w_mnlg


## -------------------------6. Nose/Tail Landing Gear----------------------------
def nlg_weight(wl, ln, nl=4.5):
    """
    Estimate nose LG weight using Raymer formula.

    TODO: (reduce total landing gear weight by 1.4%
    of TOGW if nonretractable )

    Keyword Arguments:
    wl -- Design landing weight in lb;
    ln -- Length of the nose landing gear shock strut in ft;
    nl -- (default 4.5) Ultimate landing load factor (typical range 3.5–5.5).
    """

    w_nlg = 0.125 * (nl * wl) ** 0.566 * ln**0.845

    return w_nlg


## -------------------------7. Nacelle/Cowling Weight----------------------------
def nac_weight():
    """
    Dummy equation to calculate nacelle weight;
    Nacelle weight included in installed engine.
    """

    return 0


## -------------------------8. Uninstalled (Dry) Engine--------------------------
def engine_dry_weight(p_or_t_max, engine_type):
    """
    Estimate uninstalled engine weight when the actual weight are not known.

    Keyword Arguments:
    p_or_t_max  -- If prop engine, then Pmax (BHP), if jet then Tmax (lbf)
    engine_type -- "piston", "prop", "jet"
    """

    if engine_type == "piston":
        w_eng = 50.56 + 1.352 * p_or_t_max
    elif engine_type == "prop":
        w_eng = 71.65 + 0.3658 * p_or_t_max
    elif engine_type == "jet":
        w_eng = 295.5 + 0.1683 * p_or_t_max

    return w_eng


## -------------------------9. Installed Engine----------------------------------
def engine_installed_weight(w_eng, n_eng):
    """
    Estimate installed engine weight using Raymer formula.
    (includes propeller and engine mounts).

    Keyword Arguments:
    w_eng -- Weight of each uninstalled engine in lb;
    n_eng -- Number of engines.
    """

    w_ei = 2.575 * w_eng**0.922 * n_eng

    return w_ei


## ------------------------10. Fuel System---------------------------------------
def fuel_sys_weight(qtot, qint, n_tank, n_eng):
    """
    Estimate fuel system weight using Raymer formula.

    Keyword Arguments:
    qtot   -- Total fuel quantity in US gallons;
    qint   -- Fuel quantity in integral tanks in US gallons;
    n_tank -- Number of fuel tanks;
    n_eng  -- Number of engines.
    """

    w_fs = (
        2.49
        * qtot**0.726
        * (qtot / (qtot + qint)) ** 0.363
        * n_tank**0.242
        * n_eng**0.157
    )

    return w_fs


## -------------------------11. Flight Control System-----------------------------
def fcs_weight(lfs, bw, nz, w0):
    """
    Estimate flight control system using Raymer formula.

    Keyword Arguments:
    lfs -- Length of fuselage structure (forward bulkhead to aft frame) in ft;
    bw  -- Wingspan in ft;
    nz  -- Ultimate load factor (= 1.5 x limit load factor);
    w0  -- Design gross weight (lb).
    """
    w_ctrl = 0.053 * lfs**1.536 * bw**0.371 * (nz * w0 * 1e-4) ** 0.80

    return w_ctrl


## -------------------------12. Hydraulic-----------------------------------------
def hydraulic_weight(w0, hyd_type, mach_max):
    """
    Estimate hydraulic system using Raymer formula.

    The weight of the hydraulic systems for the flight controls
    is usually included in the Flight Control System,
    so the following expression is for the other components.

    Keyword Arguments:
    w0       -- Design gross weight (lb);
    hyd_type -- "low", "medium", "high", "light" in term of speed
    in subsonic regime;
    mach     -- Mach number (design maximum)
    """

    # w_hyd = 0.001 * w0 (from Snorri)

    if hyd_type == "low":
        kh = 0.05
    elif hyd_type == "medium":
        kh = 0.11
    elif hyd_type == "high":
        kh = 0.12
    elif hyd_type == "light":
        kh = 0.013
        mach_max = 0.1

    w_hyd = kh * w0**0.8 * mach_max**0.5

    return w_hyd


## -------------------------13. Avionics Systems----------------------------------
def avionics_weight(w_uav):
    """
    The expression below assumes analog dials and overpredicts
    the weight of modern electronic flight instrument
    system (EFIS).

    Keyword Arguments:
    w_uav -- Weight of the uninstalled avionics in lb
             (typically = 800-1400 lb)
    """

    w_av = 2.117 * w_uav**0.933

    return w_uav


## -------------------------14. Electrical Systems--------------------------------
def electric_weight(wfs, wav):
    """
    Comprises all electric wiring for lights, instruments,
    avionics, fuel system, climate control, and so forth.

    Using Raymer/USAF formula.

    Keyword Arguments:
    wfs -- Predicted fuel system weight;
    wav -- Predicted weight of the avionics installation;
    """

    w_el = 12.57 * (wfs + wav) ** 0.51

    return w_el


## ---------15. Air Conditioning, Pressurization, and Antiicing-------------------
def aircond_weight(w0, n_occ, wav, mach_max):
    """
    Air conditioning includes both cooling and heating of
    the cabin volume. Pressurization system usually consists
    of various equipment (outflow and relief valves, pressure
    regulators, compressors, heat exchangers, and ducting).
    Antiicing systems included are either pneumatic inflat-
    able boots or bleed air heated elements.

    Keyword Arguments:
    w0        -- Design gross weight (lb);
    n_occ     -- Number of occupants (crew and passengers);
    wav       -- Predicted weight of the avionics installation;
    mach_max  -- Maximum design Mach number.
    """

    w_ac = 0.265 * w0**0.52 * n_occ**0.68 * wav**0.17 * mach_max**0.08

    return w_ac


## -------------------------16. Furnishing----------------------------------------
def furn_weight(w0):
    """

    Includes seats, insulation, sound proofing, lighting,
    galley, lavatory, overhead hat-racks, emergency equip-
    ment, and associated electric systems.

    Using Raymer formula.

    Keyword Arguments:
    w0    -- Design gross weight (lb).
    """

    w_furn = 0.0582 * w0 - 65

    return w_furn

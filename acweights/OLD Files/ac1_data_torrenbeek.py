#!/usr/bin/env python

import numpy as np

# ----------------------------------------------#
# Example AIAA 2019-2020 Undergraduate Team RFP #
# ----------------------------------------------#


## -------------------------1. WING----------------------------------------------


# Area (ft^2)
SW = 3880

# Weight of fuel in wing (lb)
WFW = 61000

# Wing Aspect Ratio
ARW = 8.7

# Wing sweep at 25% MGC (radian)
SWEEP4_WING = np.radians(25)  # radian

# Wing sweep at 50% MGC (radian)
SWEEP2_WING = np.radians(25)  # radian

# Dynamic pressure at cruise (lbf/ft2)
VC = 788.81
RHO_FL32 = 0.026560637391482848
Q = 0.5 * RHO_FL32 * VC**2

# Wing Taper Ratio
TR_WING = 0.15

# Wing thickness-to-chord ratio (maximum)
T2C_WING = 0.12

# Design gross weight (lb)
W0 = 443000

# Limit Load Factor
LLF = 2.5
# Ultimate load factor (1.5 x limit load factor)
NZ = 1.5 * LLF

# Max thickness of the wing root chord (ft)
CR = 45.917  # ft
TW_MAX = CR * T2C_WING

# Wing type (only for Cessna Method): "cantilever" or  "strut-braced"
WING_TYPE = "cantilever"

# Wingspan (ft)
BW = 183.7  # np.sqrt(ARW * SW)

# Maximum level airspeed at S-L (knot/KEAS)
VH = 275  # KTAS


## -------------------------2. Horizontal Tail-----------------------------------
# Horizontal Tail Area (ft^2)
SHT = 1054.8

# HT Aspect Ratio
# AR_HT = 5

# HT sweep at 25% MGC (radian)
SWEEP4_HT = np.radians(30)

# HT Taper ratio
TR_HT = 0.3

# ## -------------------------3. Vertical Tail-----------------------------------

# FTail: 0 for conventional tail, 1 for T-tail;
F_TAIL = 0


# Vertical Tail Area (ft^2)
SVT = 468.3

# VT sweep at 25% MGC (radian)
SWEEP4_VT = np.radians(35)

# VT Taper ratio
TR_VT = 0.31


## -------------------------4. Fuselage------------------------------------------
# Fuselage wetted area in ft2
SFUS = 12270

# Horizontal tail arm, from wing c/4 to HT c/4 (ft)
LHT = 76.65

# Length of fuselage structure (forward bulkhead to aft frame) (ft)
LFS = 203

# Depth of fuselage structure (ft)
DFS = 21

#
DFUS = 21

# Volume of pressurized cabin section (ft^3)
VP = 0.25 * np.pi * DFUS**2 * LFS

# Cabin pressure differential (psi)
DELTA_P = 7.78


## -------------------------5. Main Landing Gear---------------------------------
# Design landing weight in lb;
WL = W0

# Length of the main landing gear shock strut (ft);
# Shock strut length is the distance between the upper
# attachment point and the center of the wheel axis
LM = 11.4


# (default 4.5) Ultimate landing load factor (typical range 3.5–5.5).
NL = 4.5

# For Torrenbeek
# Wing position: "low" or "high"
WING_POS = "low"

# Landing gear type: "fixed" or "retract"
LG_TYPE = "retract"

# Class of a/c: "bizjet" or "civil"
AC_CLASS = "civil"


## -------------------------6. Nose/Tail Landing Gear----------------------------
# Length of the nose landing gear shock strut (ft)
LN = 10.5

## -------------------------7. Nacelle/Cowling Weight----------------------------
# Nacelle weight included in installed engine.
# For Torrenbeek
# "stp" (Single-engine tractor propeller), "multihop", "rp" (radial piston),
# "turboprop" (Multi-engine turboprop), "podjet" (Podded turbojet or-fan), "hbpr" (HBPR turbofan on a pylon)
NAC_ENGINE_TYPE = "hbpr"


## -------------------------8. Uninstalled (Dry) Engine--------------------------
# If prop engine, then Pmax (BHP), if jet then Tmax (lbf)
P_OR_T_MAX = 100000
ENGINE_TYPE = "jet"
# Ref value= 12200 lb

## -------------------------9. Installed Engine----------------------------------
# Number of engines
N_ENG = 2
# For Torrenbeek
WPROP = 0  # Sin it's a turbofan
PMAX = P_OR_T_MAX * (1.15078 * VH) / 375  # Conert lbf to HP


# Weight of each uninstalled engine in lb
W_ENG = 12200


## ------------------------10. Fuel System---------------------------------------

# Fuel quantity in integral tanks (US gallons)
# An integral fuel tank is defined as primary aircraft structure,
# usually wing or fuselage, that is sealed to contain fuel,
# as opposed to a rubberized fuel cell mounted in aircraft structure
QINT = 20350
# Total fuel quantity in (US gallons)
# QTOT = QINT + Additional Fuel Tank
QTOT = 20350

# Number of fuel tanks;
# e.g. wing, center, surge, and vent
N_TANK = 4

# For Torreenbeek
# "single-piston", "multi-piston", "turbo-integral", "turbo-bladder"
ENGINE_CONF = "turbo-integral"


## -------------------------11. Flight Control System-----------------------------
# No new input is needed
# For Torreenbeek
# ctrl_sys_type -- "manual-single", "manual", "powered".
CTRL_SYS_TYPE = "powered"

## -------------------------12. Hydraulic-----------------------------------------
# Air speed in subsonic regime: "low", "medium", "high", "light"
HYD_TYPE = "high"

# Mach number (design maximum)
MACH_MAX = 0.9


## -------------------------13. Avionics Systems----------------------------------
# Weight of the uninstalled avionics in lb (typically = 800-1400 lb);
# For smaller a/c for smaller aircraft to weigh 45 to 50 lb.
# This can be found from the avionics manufacturer data sheet.
W_UAV = 1100

## -------------------------14. Electrical Systems--------------------------------
# No new input is needed
# For Torrenbeek
# Target useful load (lb)
WU = 93400


## ---------15. Air Conditioning, Pressurization, and Antiicing-------------------
N_CABIN_CREW = 8
N_FLIGHT_CREW = 2
N_PAX = 400
N_OCC = N_CABIN_CREW + N_FLIGHT_CREW + N_PAX


## -------------------------16. Furnishing----------------------------------------
# No new input is needed

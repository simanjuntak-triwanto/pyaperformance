#!/usr/bin/env python3

import wanalysis.usaf as wusaf

# from ac1_data_usaf import *

from ac1_data import *

## -------------------------1. WING----------------------------------------------
wing_weight = wusaf.wing_weight(NZ, W0, ARW, SWEEP4_WING, SW, TR_WING, T2C_W, VH)

## -------------------------2. Horizontal Tail-----------------------------------
ht_weight = wusaf.ht_weight(NZ, W0, SHT, LHT, BHT, THT_MAX)

# -------------------------3. Vertical Tail-------------------------------------
vt_weight = wusaf.vt_weight(F_TAIL, NZ, W0, SVT, BVT, TVT_MAX)

## -------------------------4. Fuselage------------------------------------------
fus_weight = wusaf.fus_weight(NZ, W0, LF, WF, DF, VH)

## -------------------------5. Main Landing Gear---------------------------------
mnlg_weight = wusaf.mnlg_weight(WL, LM, NL)

## -------------------------6. Nose/Tail Landing Gear----------------------------
# Included in the main landing gear

# ## -------------------------7. Nacelle/Cowling Weight----------------------------
# Included in the installed engine


## -------------------------8. Uninstalled (Dry) Engine--------------------------
engine_dry_weight = wusaf.engine_dry_weight(P_OR_T_MAX, ENGINE_TYPE)

## -------------------------9. Installed Engine----------------------------------
# W_NAC = nac_weight
engine_installed_weight = wusaf.engine_installed_weight(W_ENG, N_ENG)


## ------------------------10. Fuel System---------------------------------------
fuel_sys_weight = wusaf.fuel_sys_weight(QTOT, QINT, N_TANK, N_ENG)

## -------------------------11. Flight Control System-----------------------------
fcs_weight = wusaf.fcs_weight(W0, CTRL_SYS_TYPE)

## -------------------------12. Hydraulic-----------------------------------------
hydraulic_weight = wusaf.hydraulic_weight(W0)

## -------------------------13. Avionics Systems----------------------------------
avionics_weight = wusaf.avionics_weight(W_UAV)

# ## -------------------------14. Electrical Systems--------------------------------
WAV = avionics_weight
WFS = fuel_sys_weight
electric_weight = wusaf.electric_weight(WFS, WAV)

## ---------15. Air Conditioning, Pressurization, and Antiicing-------------------
aircond_weight = wusaf.aircond_weight(W0, N_OCC, WAV, MACH_MAX)

## -------------------------16. Furnishing----------------------------------------
furn_weight = wusaf.furn_weight(N_CREW, QH)


print(
    int(wing_weight),
    # int(emp_weight),
    int(ht_weight),
    int(vt_weight),
    int(fus_weight),
    int(mnlg_weight),
    # int(nlg_weight),
    # int(nac_weight),
    int(engine_dry_weight),
    int(engine_installed_weight),
    int(fuel_sys_weight),
    int(fcs_weight),
    int(hydraulic_weight),
    int(avionics_weight),
    int(electric_weight),
    int(aircond_weight),
    int(furn_weight),
)

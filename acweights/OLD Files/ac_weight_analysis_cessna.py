#!/usr/bin/env python3

import wanalysis.cessna as wcess

from ac1_data import *

## -------------------------1. WING----------------------------------------------
wing_weight = wcess.wing_weight(NZ, W0, SW, ARW, WING_TYPE)

## -------------------------2. Horizontal Tail-----------------------------------
ht_weight = wcess.ht_weight(W0, SHT, AR_HT, THT_MAX)

# -------------------------3. Vertical Tail-------------------------------------
vt_weight = wcess.vt_weight(F_TAIL, W0, SVT, AR_VT, TVT_MAX, SWEEP4_VT)

## -------------------------4. Fuselage------------------------------------------
fus_weight = wcess.fus_weight(W0, RMAX, LFS, WING_POS, N_OCC)

## -------------------------5. Main Landing Gear---------------------------------
mnlg_weight = wcess.mnlg_weight(W0, WL, LM, NZ, WING_POS, NL)

# ## -------------------------6. Nose/Tail Landing Gear----------------------------
# # Included in the main landing gear

# ## -------------------------7. Nacelle/Cowling Weight----------------------------
nac_weight = wcess.nac_weight(PMAX, N_ENG, PISTON_ENGINE_TYPE)

## -------------------------8. Uninstalled (Dry) Engine--------------------------
engine_dry_weight = wcess.engine_dry_weight(P_OR_T_MAX, ENGINE_TYPE)

# ## -------------------------9. Installed Engine----------------------------------
W_NAC = nac_weight
engine_installed_weight = wcess.engine_installed_weight(PMAX, WPROP, N_ENG, W_NAC)


## ------------------------10. Fuel System---------------------------------------
fuel_sys_weight = wcess.fuel_sys_weight(QTOT, FUEL_SYS_TYPE)

## -------------------------11. Flight Control System-----------------------------
fcs_weight = wcess.fcs_weight(W0)

## -------------------------12. Hydraulic-----------------------------------------
hydraulic_weight = wcess.hydraulic_weight(W0)

## -------------------------13. Avionics Systems----------------------------------
avionics_weight = wcess.avionics_weight(W_UAV)

# ## -------------------------14. Electrical Systems--------------------------------
WAV = avionics_weight
electric_weight = wcess.electric_weight(W0)

## ---------15. Air Conditioning, Pressurization, and Antiicing-------------------
aircond_weight = wcess.aircond_weight(W0, N_OCC, WAV, MACH_MAX)

## -------------------------16. Furnishing----------------------------------------
furn_weight = wcess.furn_weight(N_OCC, W0)


print(
    int(wing_weight),
    # int(emp_weight),
    int(ht_weight),
    int(vt_weight),
    "fuselage weight (lb) = " + str(int(fus_weight)),
    int(mnlg_weight),
    # # int(nlg_weight),
    int(nac_weight),
    int(engine_dry_weight),
    int(engine_installed_weight),
    int(fuel_sys_weight),
    int(fcs_weight),
    int(hydraulic_weight),
    int(avionics_weight),
    int(electric_weight),
    int(aircond_weight),
    int(furn_weight),
)

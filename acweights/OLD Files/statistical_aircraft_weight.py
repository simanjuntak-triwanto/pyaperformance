#!/usr/bin/env python3

import numpy as np

# Example 6-5
SW = 130  # ft^2
WFW = 100  # lbf
ARW = 8
WING_SWEEP_RATIO_4 = np.radians(0)  # radians
WING_SWEEP_RATIO_2 = np.radians(-2.386)  # degree
Q = 33.9  # lbf/ft^2
WING_TR = 0.5
WING_T2C = 0.16
W0 = 1320  # lbf
NZ = 1.5 * 4
CR = 5.375  # ft
TW_MAX = CR * WING_T2C  # ft
WING_TYPE = "cantilever"
BW = np.sqrt(ARW * SW)  # ft, wing span
VH = 200  # KTAS


#############################################################
## 1. Wing
#############################################################


def wing_weight_cessna(nz, w0, sw, arw, w_type):
    """
    Estimate wing weight using cessna formula;
    valid only VH <= 200 KTAS (Maximum level airspeed at S-L in KEAS).

    Keyword Arguments:
    nz        -- Ultimate load factor (= 1.5 x limit load factor);
    w0        -- Design gross weight (lbf);
    sw        -- Trapezoidal wing area in (ft2);
    arw       -- Aspect Ratio of wing;
    wing type -- "cantilever" or "strut-braced"
    """

    if w_type == "cantilever":
        w_w = 0.04674 * (nz * w0) ** 0.397 * sw**0.360 * arw**1.712
    elif w_type == "strut-braced":
        w_w = 0.002933 * nz**0.611 * sw**1.018 * arw**2.473

    return w_w


def wing_weight_raymer(sw, wfw, arw, wing_sweep_4, q, tr_wing, t2c_w, nz, w0):
    """
    Estimate wing weight using Raymer formula;

    Keyword Arguments:
    sw           -- Trapezoidal wing area in (ft2);
    wfw          -- Weight of fuel in wing in lbf. (If wfw=0 then let
                    w^0.0035);
    arw          -- Aspect Ratio of wing;
    wing_sweep_4 -- Wing sweep at 25% MGC;
    q            -- Dynamic pressure at cruise (lbf/ft2);
    tr_wing  -- Taper ratio of wing;
    t2c_w          -- Wing thickness-to-chord ratio (maximum);
    nz           -- Ultimate load factor (= 1.5 x limit load factor);
    w0           -- Design gross weight (lbf);
    """

    w_w = (
        0.036
        * sw**0.758
        * wfw**0.0035
        * (arw / np.cos(wing_sweep_4) ** 2) ** 0.6
        * q**0.006
        * tr_wing**0.004
        * (100 * t2c_w / np.cos(wing_sweep_4)) ** (-0.3)
        * (nz * w0) ** 0.49
    )

    return w_w


def wing_weight_torenbeek(w0, bw, sw, wing_sweep_2, nz, tw_max):
    """
    Estimate wing weight using Torrenbeek formula;

    Keyword Arguments:
    w0           -- Design gross weight (lbf);
    bw           -- Wingspan in (ft);
    SW           -- Trapezoidal wing area in ft2
    wing_sweep_2 -- Wing sweep at 50% MGC;
    nz           -- Ultimate load factor (= 1.5 x limit load factor);
    tw_max       -- Max thickness of the wing root chord in ft.
    """

    w_w = (
        0.00125
        * w0
        * (bw / np.cos(wing_sweep_2)) ** 0.75
        * (1 + np.sqrt(6.3 * np.cos(wing_sweep_2) / bw))
        * nz**0.55
        * (bw * sw / (tw_max * w0 * np.cos(wing_sweep_2))) ** 0.30
    )

    return w_w


def wing_weight_usaf(nz, w0, arw, wing_sweep_4, sw, tr_wing, t2c_w, vh):
    """
    Estimate wing weight using USAF formula;
    valid for vh <= 300 KTAS.

    Keyword Arguments:
    nz           -- Ultimate load factor (= 1.5 x limit load factor);
    w0           -- Design gross weight (lbf);
    arw          -- Aspect Ratio of wing;
    wing_sweep_4 -- Wing sweep at 25% MGC;
    sw           -- Trapezoidal wing area in (ft2);
    tr_wing      -- Taper ratio of wing;
    t2c_w        -- Wing thickness-to-chord ratio (maximum);
    vh           -- Maximum level airspeed at S-L in KEAS;
    """

    if vh > 300:
        raise Exception("USAF formula nly valid for vh <= 300 KTAS")

    w_w = (
        96.948
        * (
            (nz * w0 / 1e5) ** 0.65
            * (arw / np.cos(wing_sweep_4) ** 2) ** 0.57
            * (sw / 100) ** 0.61
            * ((1 + tr_wing) / (2 * t2c_w)) ** 0.36
            * np.sqrt(1 + vh / 500)
        )
        * 0.993
    )

    return w_w


#############################################################
## 2. Horizontal Tail
#############################################################


def ht_weight_cessna(w0, sht, arht, tht_max):
    """
    Estimate the horizontal tail weight using Cessna formula.;
    valid for vh <= 200 KTAS.

    Keyword Arguments:
    w0      -- Design gross weight (lbf);
    sht     -- Trapezoidal HT area in ft2;
    arht    -- Aspect Ratio of HT;
    tht_max -- Max root chord thickness of HT in ft;
    """

    w_ht = (
        3.184 * w0**0.887 * sht**0.101 * arht**0.138 / (174.04 * tht_max**0.223)
    )

    return w_ht


def ht_weight_raymer(nz, w0, q, sht, t2c_ht, ar_ht, ht_sweep_4, tr_ht):
    """
    Estimate the horizontal tail weight using Raymer formula.

    Keyword Arguments:
    nz         -- Ultimate load factor (= 1.5 x limit load factor);
    w0         -- Design gross weight (lbf);
    q          -- Dynamic pressure at cruise (lbf/ft2);
    sht        -- Trapezoidal HT area in ft2;
    t2c_ht     -- HT thickness-to-chord ratio (maximum);
    ar_ht      -- AR of HT;
    ht_sweep_4 -- HT sweep ratio at 25% MGC;
    tr_ht      -- HT taper ratio.
    """

    w_ht = (
        0.16
        * (nz * w0) ** 0.414
        * q**0.168
        * sht**0.896
        * (100 * t2c_ht / np.cos(ht_sweep_4)) ** (-0.12)
        * (ar_ht / np.cos(ht_sweep_4) ** 2) ** 0.043
    )

    return w_ht


def emp_weight_torrenbeek(nz, sht, svt):
    """
    Estimate the emmpenage weight using Torenbeek formula.

    Keyword Arguments:
    nz  -- Ultimate load factor (= 1.5 x limit load factor);
    sht -- Trapezoidal HT area in ft2;
    svt -- Trapezoidal VT area in ft2;
    """

    w_emp = 0.04 * (nz * (sht + svt) ** 2) ** 0.75

    return w_emp


def ht_weight_usaf(nz, w0, sht, lht, bht, tht_max):
    """
    Estimate the horizontal tail weight using USAF formula.

    Keyword Arguments:
    nz      -- Ultimate load factor (= 1.5 x limit load factor);
    w0      -- Design gross weight (lbf);
    sht     -- Trapezoidal HT area in ft2;
    lht     -- Horizontal tail arm, from wing c/4 to HT c/4 in ft;
    bht     -- HT span in ft;
    tht_max -- Max root chord thickness of HT in ft.
    """

    w_ht = (
        71.927
        * (
            (nz * w0 / 1e5) ** 0.87
            * (sht / 100) ** 1.2
            * (lht / 10) ** 0.483
            * np.sqrt(bht / tht_max)
        )
        * 0.458
    )

    return w_ht


#############################################################
## 3. Vertical Tail
#############################################################


def vt_weight_cessna(f_tail, w0, svt, ar_vt, tvt_max, vt_sweep_4):
    """
    Estimate the horizontal tail weight using Cessna formula.

    Keyword Arguments:
    f_tail     -- 0 for conventional tail, ¼1 for T-tail;
    w0         -- Design gross weight (lbf);
    svt        -- Trapezoidal VT area in ft2;
    ar_vt      -- AR of VT;
    tvt_max    -- Max root chord thickness of VT in ft;
    vt_sweep_4 -- VT sweep at 25% MGC.
    """

    w_vt = (
        (1 + 0.2 * f_tail)
        * (1.68 * w0**0.567 * svt**0.1249 * ar_vt**0.482)
        / (639.95 * tvt_max**0.747 * np.cos(vt_sweep_4) ** 0.882)
    )

    return w_vt


def vt_weight_raymer(f_tail, nz, w0, q, svt, t2c_vt, vt_sweep_4, ar_vt, tr_vt):
    """
    Estimate the horizontal tail weight using Raymer formula.

    Keyword Arguments:
    f_tail     -- 0 for conventional tail, ¼1 for T-tail;
    nz         -- Ultimate load factor (= 1.5 x limit load factor);
    w0         -- Design gross weight (lbf);
    q          -- Dynamic pressure at cruise (lbf/ft2);
    svt        -- Trapezoidal VT area in ft2;
    t2c_vt     -- VT thickness-to-chord ratio (maximum);
    vt_sweep_4 -- VT sweep at 25% MGC.
    ar_vt      -- AR of VT;
    tr_vt      -- VT taper ratio.
    """

    w_vt = (
        0.073
        * (1 + 0.2 * f_tail)
        * (nz * w0) ** 0.376
        * q**0.122
        * svt**0.873
        * (100 * t2c_vt / np.cos(vt_sweep_4)) ** (-0.49)
        * (ar_vt / np.cos(vt_sweep_4) ** 0.357)
        * tr_vt**0.039
    )

    return w_vt


def vt_weight_usaf(f_tail, nz, w0, svt, bvt, tvt_max):
    """
    Estimate the horizontal tail weight using USAF formula.

    Keyword Arguments:
    f_tail  -- 0 for conventional tail, ¼1 for T-tail;
    nz      -- Ultimate load factor (= 1.5 x limit load factor);
    w0      -- Design gross weight (lbf);
    svt     -- Trapezoidal VT area in ft2;
    bvt     -- VT span in ft;
    tvt_max -- Max root chord thickness of VT in ft.
    """

    w_vt = (
        55.786
        * (1 + 0.2 * f_tail)
        * ((nz * w0 / 1e5) ** 0.87 * (svt / 100) * np.sqrt(bvt / tvt_max)) ** 0.458
    )

    return w_vt


#############################################################
## 4. Fuselage
#############################################################
def fus_weight_cessna(w0, rmax, lfs, wing_pos, nocc=1):
    """
    Estimate fuselage weight using cessna formula;
    valid for vh <= 200 KTAS.

    Keyword Arguments:
    w0       -- Design gross weight (lbf);
    rmax     -- Fuselage maximum perimeter in ft;
    lfs      -- Length of fuselage structure (forward bulkhead to aft frame) in ft;
    wing_pos -- Wing position "low" or "high";
    nocc     -- (default 1 for UAV) Number of occupants (crew and passengers).
    """

    if nocc == "low":
        w_fus = 0.04682 * w0**0.692 * rmax**0.374 * lfs**0.590
    elif nocc == "high":
        w_fus = (
            14.86 * w0**0.144 * (lfs / rmax) ** 0.778 * lfs**0.383 * nocc**0.455
        )

    return w_fus


def fus_weight_raymer(sfus, nz, w0, lht, lfs, dfs, vp, delta_p=8):
    """
    Estimate fuselage weight using Raymer formula.

    Keyword Arguments:
    sfus    -- Fuselage wetted area in ft2
    nz      -- Ultimate load factor (= 1.5 x limit load factor);
    w0      -- Design gross weight (lbf);
    lht     -- Horizontal tail arm, from wing c/4 to HT c/4 in ft;
    lfs     -- Length of fuselage structure (forward bulkhead to aft frame) in ft;
    dfs     -- Depth of fuselage structure in ft;
    vp      -- Volume of pressurized cabin section in ft3;
    delta_p -- (default 8 psi) Cabin pressure differential, in psi (typically 8 psi).
    """

    w_fus = (
        0.052
        * sfus**1.086
        * (nz * w0) ** 0.177
        * lht
        * (-0.051)
        * (lfs / dfs) ** (-0.072)
        * q**0.241
        + 11.9 * vp * delta_p**0.271
    )

    return w_fus


def fus_weight_usaf(nz, w0, lf, wf, df, vh):
    """
    Estimate fuselage weight using usaf formula.

    Keyword Arguments:
    nz -- Ultimate load factor (= 1.5 x limit load factor);
    w0 -- Design gross weight (lbf);
    lf -- Fuselage length in ft;
    wf -- Fuselage max width in ft;
    df -- Fuselage max depth in ft;
    vh -- Maximum level airspeed at S-L in KEAS.
    """

    w_fus = (
        200
        * (
            (nz * w0 / 1e5) ** 0.286
            * (lf / 10) ** 0.857
            * ((wf + df) / 10)
            * (vh / 100) ** 0.338
        )
        ** 1.1
    )

    return w_fus


#############################################################
## 5. Main Landing Gear
#############################################################


def mnlg_weight_cessna(w0, wl, lm, nz, wing_pos, nl=4.5):
    """
    Estimate main LG weight using usaf Cesna formula;
    valid for vh <= 200 KTAS.


    Keyword Arguments:
    w0       -- Design gross weight (lbf);
    wl       -- Design landing weight in lbf;
    lm       -- Length of the main landing gear shock strut in ft;
    nz       -- Ultimate load factor (= 1.5 x limit load factor);
    wing_pos -- "low" or "high";
    nl       -- (default 4.5) Ultimate landing load factor (typical range 3.5–5.5).
    """
    w_mnlg_low = (
        6.2
        + 0.0143 * w0
        + 0.362 * wl**0.417 * nl**0.950 * lm**0.183
        + 0.007157 * wl**0.749 * nz * ln * 0.788
    )

    w_mnlg_high = (
        6.2
        + 0.0283 * w0
        + 0.362 * wl**0.417 * nl**0.950 * lm**0.183
        + 0.007157 * wl**0.749 * nz * ln * 0.788
    )

    if wing_pos == "low":
        w_mnlg = w_mnlg_low
    elif wing_pos == "high":
        w_mnlg = w_mnlg_high

    return w_mnlg


def mnlg_weight_raymer(wl, lm, nl=4.5):
    """
    Estimate main LG weight using Raymer formula.

    Keyword Arguments:
    wl -- Design landing weight in lbf;
    lm -- Length of the main landing gear shock strut in ft;
    nl -- (default 4.5) Ultimate landing load factor (typical range 3.5–5.5).
    """

    w_mnlg = 0.095 * (nl * wl) ** 0.768 * lm**0.409

    return w_mnlg


def mnlg_weight_torrenbeek(w0, wing_pos, lg_type, ac_class):
    """
    Estimate main LG weight using torrenbeek formula.

    Keyword Arguments:
    w0       -- Design gross weight (lbf);
    wing_pos -- "low" or "high"
    lg_type  -- "fixed" or "retract"
    ac_class -- "bizjet" or "civil"
    """
    if ac_class == "bizjet":
        A = 33
        B = 0.04
        C = 0.021
        D = 0
    elif ac_class == "civil":
        if lg_type == "fixed":
            A = 20
            B = 0.10
            C = 0.019
            D = 0
        elif lg_type == "retract":
            A = 40
            B = 0.16
            C = 0.019
            D = 1.5 * 1e-5

    if wing_pos == "low":
        w_mnlg = A + B * w0**0.75 + C * w0 + D * w0**1.5
    elif wing_pos == "high":
        w_mnlg = 1.08 * (A + B * w0**0.75 + C * w0 + D * w0**1.5)

    return w_mnlg


def mnlg_weight_uasf(wl, lm, nl=4.5):
    """
    Estimate main LG weight using usaf formula;
    valid for vh <= 200 KTAS.

    Keyword Arguments:
    wl -- Design landing weight in lbf;
    lm -- Length of the main landing gear shock strut in ft;
    nl -- (default 4.5) Ultimate landing load factor (typical range 3.5–5.5).
    """

    w_mnlg = 0.054 * (nl * wl) ** 0.684 * lm**0.501

    return w_mnlg


#############################################################
## 6. Nose + Tail Landing Gear
#############################################################


def nlg_weight_raymer(wl, ln, nl=4.5):
    """
    Estimate nose LG weight using Raymer formula.
    Keyword Arguments:
    wl -- Design landing weight in lbf;
    ln -- Length of the main landing gear shock strut in ft;
    nl -- (default 4.5) Ultimate landing load factor (typical range 3.5–5.5).
    """

    w_nlg = 0.125 * (nl * wl) ** 0.566 * ln**0.845

    return w_nlg


def nlg_weight_torrenbeek(w0, wing_pos, lg_type, ac_class):
    """
    Estimate tail LG weight using torrenbeek formula.

    Keyword Arguments:
    w0       -- Design gross weight (lbf);
    wing_pos -- "low" or "high"
    lg_type  -- "fixed" or "retract"
    ac_class -- "bizjet" or "civil"
    """
    if ac_class == "bizjet":
        A = 12
        B = 0.06
        C = 0
        D = 0
    elif ac_class == "civil":
        if lg_type == "fixed":
            A = 25
            B = 0
            C = 0.0024
            D = 0
        elif lg_type == "retract":
            A = 20
            B = 0.10
            C = 0
            D = 2.0 * 1e-6

    if wing_pos == "low":
        w_nlg = A + B * w0**0.75 + C * w0 + D * w0**1.5
    elif wing_pos == "high":
        w_nlg = 1.08 * (A + B * w0**0.75 + C * w0 + D * w0**1.5)

    return w_nlg


def tlg_weight_torrenbeek(w0, wing_pos, lg_type, ac_class):
    """
    Estimate tail LG weight using torrenbeek formula.

    Keyword Arguments:
    w0       -- Design gross weight (lbf);
    wing_pos -- "low" or "high"
    lg_type  -- "fixed" or "retract"
    ac_class -- "bizjet" or "civil"
    """
    if ac_class == "bizjet":
        A = 0
        B = 0
        C = 0
        D = 0
    elif ac_class == "civil":
        if lg_type == "fixed":
            A = 9
            B = 0
            C = 0.0024
            D = 0
        elif lg_type == "retract":
            A = 5
            B = 0
            C = 0.0031
            D = 0

    if wing_pos == "low":
        w_tlg = A + B * w0**0.75 + C * w0 + D * w0**1.5
    elif wing_pos == "high":
        w_tlg = 1.08 * (A + B * w0**0.75 + C * w0 + D * w0**1.5)

    return w_tlg


#############################################################
## 7. Nacelle/Cowling Weight
#############################################################


def nac_weight_cessna(pmax, n_eng, engine_type):
    """
    Estimate the weight of nacelles or cowlings using Cessna formula.

    Keyword Arguments:
    pmax        -- Maximum rated power per engine in BHP or ESHP
    n_eng       -- Number of engines
    engine_type -- "rpe" (radial piston engine) or "hop" (horizontally opposed piston engine)
    """
    if engine_type == "rpe":
        w_nac = 0.37 * pmax * n_eng
    elif engine_type == "hop":
        w_nac = 0.24 * pmax * n_eng

    return w_nac


def nac_weight_torrenbeek(pmax, tmax, n_eng, engine_type):
    """
    Estimate the weight of nacelles or cowlings using Torrenbeek formula.
    For prop engines set tmax = 0, and for jet engines, set pmax=0.

    Keyword Arguments:
    pmax        -- Maximum rated power per engine in BHP or ESHP;
    tmax        -- Maximum rated thrust per engine in lbf;
    n_eng       -- Number of engines
    engine_type -- "stp" (Single-engine tractor propeller), "multihop", "rp" (radial piston),
                   "turboprop", "podjet", "hbpr"
    """
    if engine_type == "stp":
        w_nac = 2.5 * np.sqrt(pmax)
    elif engine_type == "multihop":
        w_nac = 0.32 * pmax * n_eng
    elif engine_type == "multirp":
        w_nac = 0.045 * pmax**1.25 * n_eng
    elif engine_type == "turboprop":
        w_nac = 0.14 * pmax * n_eng
    elif engine_type == "podjet":
        w_nac = 0.055 * tmax
    elif engine_type == "hbpr":
        w_nac = 0.065 * tmax

    return w_nac


#############################################################
## 8. Uninstalled (Dry) Engine Weight
#############################################################


def engine_dry_weight(p_or_t_max, engine_type):
    """
    Estimate uninstalled engine weight when the actual weight are not known.

    Keyword Arguments:
    p_or_t_max  -- If prop engine, then Pmax (BHP), if jet then Tmax (lbf)
    engine_type -- "piston", "prop", "jet"
    """

    if engine_type == "piston":
        w_eng = 50.56 + 1.352 * p_or_t_max
    elif engine_type == "prop":
        w_eng = 71.65 + 0.3658 * p_or_t_max
    elif engine_type == "jet":
        w_eng = 295.5 + 0.1683 * p_or_t_max

    return w_eng


#############################################################
## 9. Installed Engine Weight
#############################################################


def engine_installed_weight_cessna(pmax, wprop, n_eng, w_nac):
    """
    Estimate installed engine weight using Cessna formula.

    Keyword Arguments:
    pmax  -- Maximum rated power per engine in BHP;
    wprop -- Propeller weight (set wprop=0 for jet);;
    n_eng -- Number of engines;
    w_nac -- Predicted weight of all engine nacelles in lbf.
    """
    w_ei = (1.3 * pmax + wprop) * n_eng + w_nac

    return w_ei


def engine_installed_weight_raymer(w_eng, n_eng):
    """
    Estimate installed engine weight using Raymer formula.

    Keyword Arguments:
    w_eng -- Weight of each uninstalled engine in lbf;
    n_eng -- Number of engines.
    """

    w_ei = 2.575 * w_eng**0.922 * n_eng

    return w_ei


def engine_installed_weight_torrenbeek(w_eng, wprop, n_eng, pmax, w_nac):
    """
    Estimate installed engine weight using Torrenbeek formula.

    Keyword Arguments:
    w_eng -- Weight of each uninstalled engine in lbf;
    wprop -- Propeller weight (set wprop=0 for jet);
    n_eng -- Number of engines;
    pmax  -- Maximum rated power per engine in BHP;
    w_nac -- Predicted weight of all engine nacelles in lbf.
    """

    w_ei = (w_eng + wprop) * n_eng + 1.03 * n_eng**0.3 * pmax**0.7 + w_nac

    return w_ei


def engine_installed_weight_usaf(w_eng, n_eng):
    """
    Estimate installed engine weight using USAF formula.

    Keyword Arguments:
    w_eng -- Weight of each uninstalled engine in lbf;
    n_eng -- Number of engines;
    """

    w_ei = 2.575 * w_eng * 0.922 * n_eng

    return w_ei


#############################################################
## 10. Fuel system weight
#############################################################
def fuel_sys_weight_cessna(qtot, fuel_sys_type):
    """
    Estimate installed fuel weight using Cessna formula.

    Keyword Arguments:
    qtot          -- Total fuel quantity in US gallons;
    fuel_sys_type -- "avgas-no-tip", "jeta-no-tip",
                     "avgas-tip", "jet-a-tip"
    """
    if fuel_sys_type == "avgas-no-tip":
        w_fs = 0.40 * qtot
    elif fuel_sys_type == "jeta-no-tip":
        w_fs = 0.4467 * qtot
    elif fuel_sys_type == "avgas-tip":
        w_fs = 0.70 * qtot
    elif fuel_sys_type == "jet-a-tip":
        w_fs = 0.7817 * qtot

    return w_fs


def fuel_sys_weight_raymer(qtot, qint, n_tank, n_eng):
    """
    Estimate fuel system weight using Raymer formula.

    Keyword Arguments:
    qtot   -- Total fuel quantity in US gallons;
    qint   -- Fuel quantity in integral tanks in US gallons;
    n_tank -- Number of fuel tanks;
    n_eng  -- Number of engines.
    """

    w_fs = (
        2.49
        * qtot**0.726
        * (qtot / (qtot + qint)) ** 0.363
        * n_tank**0.242
        * n_eng**0.157
    )

    return w_fs


def fuel_sys_weight_torrenbeek(
    qtot,
    engine_conf,
    n_tank=1,
    n_eng=1,
):
    """
    Estimate fuel system weight using torrenbeek formula.

    See Torrenbeek (P 286).

    Keyword Arguments:
    qtot        -- Total fuel quantity in US gallons;
    engine_conf -- "single-piston", "multi-piston", "turbo-integral"
                   "turbo-bladder"
    n_tank      -- (default 1) Number of fuel tanks;
    n_eng       -- (default 1) Number of engines.
    """

    if engine_conf == "single-piston":
        w_fs = 2 * qtot**0.667
    elif engine_conf == "multi-piston":
        w_fs = 4.5 * qtot**0.60
    elif engine_conf == "turbo-integral":
        w_fs = 80 * (n_eng + n_tank - 1) + 15 * n_tank**0.5 * qtot**0.333
    elif engine_conf == "turbo-bladder":
        w_fs = 3.2 * qtot**0.727

    return w_fs


def fuel_sys_weight_usaf(qtot, qint, n_tank_n_eng):
    """
    Estimate fuel system weight using USAF formula.

    Keyword Arguments:
    qtot         -- Total fuel quantity in US gallons;
    qint         -- Fuel quantity in integral tanks in US gallons;
    n_tank       -- Number of fuel tanks;
    n_eng        -- Number of engines.
    """

    w_fs = 2.49 * (
        qtot**0.6 * (qtot / (qtot + qint)) ** 0.3 * n_tank**0.2 * n_eng**0.13
    )

    return w_fs


#############################################################
## 11. Flight Control System Weight
#############################################################
def fcs_weight_cessna(w0):
    """
    Estimate flight control system using cessna formula
    (manual control system).

    Keyword Arguments:
    w0 -- Design gross weight (lbf);
    """
    w_ctrl = 0.0168 * w0

    return w_ctrl


def fcs_weight_raymer(lfs, bw, nz, w0):
    """
    Estimate flight control system using Raymer formula.

    Keyword Arguments:
    lfs -- Length of fuselage structure (forward bulkhead to aft frame) in ft;
    bw  -- Wingspan in ft;
    nz  -- Ultimate load factor (= 1.5 x limit load factor);
    w0  -- Design gross weight (lbf).
    """
    w_ctrl = 0.053 * lfs**1.536 * bw**0.371 * (nz * w0 * 1e-4) ** 0.80

    return w_ctrl


def fcs_weight_torrenbeek(w0, ctrl_sys_type):
    """
    Estimate flight control system using Torrenbeek formula.

    Keyword Arguments:
    w0            -- Design gross weight (lbf);
    ctrl_sys_type -- "manual-single", "manual", "powered".
    """

    if ctrl_sys_type == "manual-single":
        w_ctrl = 0.23 * w0**0.667
    elif ctrl_sys_type == "manual":
        w_ctrl = 0.44 * w0**0.667
    elif ctrl_sys_type == "powered":
        w_ctrl = 0.64 * w0**0.667

    return w_ctrl


def fcs_weight_uasf(w0, ctrl_sys_type):
    """
    Estimate flight control system using USAF formula.

    Keyword Arguments:
    w0            -- Design gross weight (lbf).
    ctrl_sys_type -- "manual", "powered".
    """
    if ctrl_sys_type == "manual":
        w_ctrl = 1.066 * w0**0.626
    elif ctrl_sys_type == "powered":
        w_ctrl = 1.08 * w0**0.7

    return w_ctrl


#############################################################
## 12. Hydraulic System Weight
#############################################################
def hydraulic_weight(w0):
    """
    Estimate hydraulic system.

    The weight of the hydraulic systems for the flight controls
    is usually included in the Flight Control System,
    so the following expression is for the other components.

    Keyword Arguments:
    w0 -- Design gross weight (lbf);
    """

    w_hyd = 0.001 * w0

    return w_hyd


#############################################################
## 13. Avionics Systems Weight
#############################################################
def avionics_weight(w_uav):
    """
    The expression below assumes analog dials and overpredicts
    the weight of modern electronic flight instrument
    system (EFIS).

    Keyword Arguments:
    w_uav -- Weight of the uninstalled avionics in lbf
    """

    w_av = 2.11 * w_uav**0.933

    return w_uav


#############################################################
## 14. Electrical System
#############################################################
def electric_weight_cessna(w0):
    """
    Comprises all electric wiring for lights, instruments,
    avionics, fuel system, climate control, and so forth.

    Using Cessna formula.

    Keyword Arguments:
    w0 -- Design gross weight (lbf);
    """

    w_el = 0.0268 * w0

    return w_el


def electric_weight_raymer(wfs, wav):
    """
    Comprises all electric wiring for lights, instruments,
    avionics, fuel system, climate control, and so forth.

    Using Raymer/USAF formula.

    Keyword Arguments:
    wfs -- Predicted fuel system weight;
    wav -- Predicted weight of the avionics installation;
    """

    w_el = 12.57 * (wfs + wav) ** 0.51

    return w_el


def electric_weight_usaf(wfs, wav):
    """
    Comprises all electric wiring for lights, instruments,
    avionics, fuel system, climate control, and so forth.

    Using Raymer/USAF formula.

    Keyword Arguments:
    wfs -- Predicted fuel system weight;
    wav -- Predicted weight of the avionics installation;
    """

    w_el = 12.57 * (wfs + wav) ** 0.51

    return w_el


def electric_weight_torenbeek(w0, wu, whyd):
    """
    Comprises all electric wiring for lights, instruments,
    avionics, fuel system, climate control, and so forth.

    Using Torrenbeek.

    Keyword Arguments:
    w0   -- Design gross weight (lbf);
    wu   -- Target useful load in lbf;
    whyd -- Predicted weight of the hydraulics system in lbf.
    """

    w_el = 0.0078 * (w0 - wu) ** 1.2 - whyd

    return w_el


#############################################################
## 15. Air Conditioning, Pressurization, and Antiicing
#############################################################
def aircond_weight(w0, n_occ, wav, mach):
    """
    Air conditioning includes both cooling and heating of
    the cabin volume. Pressurization system usually consists
    of various equipment (outflow and relief valves, pressure
    regulators, compressors, heat exchangers, and ducting).
    Antiicing systems included are either pneumatic inflat-
    able boots or bleed air heated elements.

    Keyword Arguments:
    w0    -- Design gross weight (lbf);
    n_occ -- Number of occupants (crew and passengers);
    wav   -- Predicted weight of the avionics installation;
    mach  -- Mach number.
    """

    w_ac = 0.265 * w0**0.52 * n_occ**0.68 * wav**0.17 * mach**0.08

    return w_ac


#############################################################
## 16. Furnishings
#############################################################
def furn_weight_cessna(n_occ, w0):
    """
    Includes seats, insulation, sound proofing, lighting,
    galley, lavatory, overhead hat-racks, emergency equip-
    ment, and associated electric systems.

    Using Cessna formula.

    Keyword Arguments:
    n_occ -- Number of occupants (crew and passengers);
    w0    -- Design gross weight (lbf).
    """

    w_furn = 0.0412 * n_occ**1.145 * w0**0.489

    return w_furn


def furn_weight_Raymer(w0):
    """
    Includes seats, insulation, sound proofing, lighting,
    galley, lavatory, overhead hat-racks, emergency equip-
    ment, and associated electric systems.

    Using Raymer formula.

    Keyword Arguments:
    w0    -- Design gross weight (lbf).
    """

    w_furn = 0.0582 * w0 - 65

    return w_furn


def furn_weight_usaf(n_crew, qh):
    """
    Includes seats, insulation, sound proofing, lighting,
    galley, lavatory, overhead hat-racks, emergency equip-
    ment, and associated electric systems.

    Using Cessna formula.

    Keyword Arguments:
    n_crew -- Number of crew;
    qh     -- Dynamic pressure at max level airspeed, lbf/ft2.
    """

    w_furn = 34.5 * n_crew * qh**0.25

    return w_furn


# # EXAMPLE Calculations of Wing weights

# ww_cessna_cantilver = wing_weight_cessna(NZ, W0, SW, ARW, "cantilever")
# ww_cessna_strut = wing_weight_cessna(NZ, W0, SW, ARW, "strut-braced")
# ww_raymer = wing_weight_raymer(
#     SW, WFW, ARW, WING_SWEEP_RATIO_4, Q, WING_TR, WING_T2C, NZ, W0
# )
# ww_torrenbeek = wing_weight_torenbeek(W0, BW, SW, WING_SWEEP_RATIO_2, NZ, TW_MAX)
# ww_usaf = wing_weight_usaf(NZ, W0, ARW, WING_SWEEP_RATIO_4, SW, WING_TR, WING_T2C, VH)
